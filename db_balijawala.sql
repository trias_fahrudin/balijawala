-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5168
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;



-- Dumping structure for table db_balijawala.armada
CREATE TABLE IF NOT EXISTS `armada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.armada: ~3 rows (approximately)
DELETE FROM `armada`;
/*!40000 ALTER TABLE `armada` DISABLE KEYS */;
INSERT INTO `armada` (`id`, `nama`, `gambar`, `keterangan`) VALUES
	(1, 'Toyota Innova', '1102a-inova.png', '<p>-</p>\r\n'),
	(2, 'Toyota Avanza', '30b0b-avanza.jpg', ''),
	(3, 'Suzuki APV', '9f867-suzuki-apv.png', '');
/*!40000 ALTER TABLE `armada` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.galeri
CREATE TABLE IF NOT EXISTS `galeri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.galeri: ~1 rows (approximately)
DELETE FROM `galeri`;
/*!40000 ALTER TABLE `galeri` DISABLE KEYS */;
INSERT INTO `galeri` (`id`, `nama`) VALUES
	(1, 'galeri 1');
/*!40000 ALTER TABLE `galeri` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.galeri_images
CREATE TABLE IF NOT EXISTS `galeri_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galeri_id` int(11) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `priority` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.galeri_images: ~1 rows (approximately)
DELETE FROM `galeri_images`;
/*!40000 ALTER TABLE `galeri_images` DISABLE KEYS */;
INSERT INTO `galeri_images` (`id`, `galeri_id`, `judul`, `file_name`, `priority`) VALUES
	(1, 1, NULL, '4f206-banyuwangi-327-1.jpg', NULL);
/*!40000 ALTER TABLE `galeri_images` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.kegiatan
CREATE TABLE IF NOT EXISTS `kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.kegiatan: ~9 rows (approximately)
DELETE FROM `kegiatan`;
/*!40000 ALTER TABLE `kegiatan` DISABLE KEYS */;
INSERT INTO `kegiatan` (`id`, `nama`, `slug`, `gambar`, `keterangan`) VALUES
	(1, 'Rafting', 'rafting', '3cc7a-rafting.jpg', '<p>-</p>\r\n'),
	(2, 'ATV Ride', 'atv-ride', 'e8a26-bali-atv-ride.jpg', '<p>-</p>\r\n'),
	(3, 'Scuba Diving', 'scuba-diving', 'c039c-scuba-diving-bali.jpg', '<p>We suggest&nbsp;Scuba&nbsp;diving&nbsp;better&nbsp;senorkeling&nbsp;because&nbsp;you&#39;ll likely enjoy&nbsp;interacting&nbsp;with&nbsp;the underwater&nbsp;flora&nbsp;and&nbsp;fauna. Unlike other modes of diving, which rely either on breath-hold or on air pumped from the surface, scuba divers carry their own source of breathing gas, (usually compressed air), allowing them greater freedom of movement than with an air line or diver&#39;s umbilical and longer underwater endurance than breath-hold. Scuba equipment may be open circuit, in which exhaled gas is exhausted to the surroundings, or closed or semi-closed circuit, in which the breathing gas is scrubbed to remove carbon dioxide, and the oxygen used is replenished from a supply of feed gas before being re-breathed.</p>\r\n'),
	(4, 'Jet Ski', 'jet-ski', 'e83ed-water-sports-bali-jet-ski.jpg', '<p><strong>Jet Ski&nbsp;</strong>is one of the most popular water sports by tourists. Much like a motorcycle, acceleration in a jet ski is provided by a hand powered throttle located on the right side grip. By twisting the throttle, the driver can increase power to the motor. Steering a jet ski requires a combination of pointing the front-mounted grips and maneuvering the body. Unlike a street motorcycle, a jet ski often requires significant a jet ski is part water skiing device and part speedboat, with a definite hint of motorcycle during operation. Instead of the traditional propeller or screw motor, a jet ski uses an enclosed gas-powered motor to literally push water out in a jet stream.</p>\r\n'),
	(5, 'Snorkeling', 'snorkeling', 'db3d7-water-sports-bali-snorkeling.jpg', ''),
	(6, 'Parasailing', 'parasailing', 'd9b79-water-sports-parasailing-bali.jpg', '<p><strong>Parasailing&nbsp;</strong>is one&nbsp;of the most&nbsp;attractive&nbsp;game of&nbsp;water.&nbsp;Parasailing&nbsp;is&nbsp;a game&nbsp;where you&nbsp;will use the&nbsp;water&nbsp;umbrellaparachute&nbsp;and then&nbsp;pulled by&nbsp;a speed boat.&nbsp;Altitude&nbsp;parachute&nbsp;straps&nbsp;withspeed boat&nbsp;is&nbsp;80&nbsp;meters.&nbsp;When&nbsp;you&nbsp;fly over,&nbsp;you&nbsp;willfeel the sensation of&nbsp;tension,&nbsp;adrenalinechallenge&nbsp;and&nbsp;enjoy the&nbsp;beauty of the beach</p>\r\n'),
	(7, 'Banana Boat', 'banana-boat', '27cbf-water-sports-banana-boat-bali.jpg', ''),
	(8, 'Glass Bottom Boat', 'glass-bottom-boat', 'be215-water-sports-bali-glass-bottom-boat.jpg', ''),
	(9, 'Fly Fish', 'fly-fish', '9cd1e-water-sports-fly-fish-bali.jpg', '');
/*!40000 ALTER TABLE `kegiatan` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.ketentuan_tour
CREATE TABLE IF NOT EXISTS `ketentuan_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pertanyaan` varchar(50) NOT NULL,
  `jawaban` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.ketentuan_tour: ~6 rows (approximately)
DELETE FROM `ketentuan_tour`;
/*!40000 ALTER TABLE `ketentuan_tour` DISABLE KEYS */;
INSERT INTO `ketentuan_tour` (`id`, `pertanyaan`, `jawaban`) VALUES
	(1, 'PRICE & VALIDITY', '<ol>\r\n	<li>All of the price valid from 1st April 2016&nbsp;until 31st March 2017 and subject to change with prior notice.</li>\r\n	<li>Price are net &amp;&nbsp;non commissionable, except for series group&nbsp;all of the rate will be negotiable.</li>\r\n</ol>\r\n'),
	(2, 'LIABILITY', '<ol>\r\n	<li>We consider safety first, which mean if condition and situation bad (such, rebellion, riots, landslide etc) the tour could be cancelled.</li>\r\n	<li>If the tour has to be cancelled, we will change the programs after obtaining an agreement with the client supported by a written from both side.</li>\r\n	<li>If an agreement is unreachable with the customer, we will charge for the portion of the service with has been given.</li>\r\n</ol>\r\n'),
	(3, 'RESERVATION', '<ol>\r\n	<li>Reservation should be made as earlier as possible. (Earlier reservation will be confirmed earlier).</li>\r\n</ol>\r\n'),
	(4, 'CONFIRMATION', '<ol>\r\n	<li>We will send the confirmation as soon as we get the confirmation from principal.</li>\r\n	<li>When you confirmed the deal&nbsp;(Package, tour program, activities etc). Its mean you are clearly understand what is include and exclude.</li>\r\n	<li>Don&rsquo;t hesitate to contact us by phone or by email&nbsp; for detail information before confirming your deal.</li>\r\n</ol>\r\n'),
	(5, 'CANCELLATION', '<ol>\r\n	<li>Cancellation less than 10 days before departure will be charge 50% of tour price.</li>\r\n	<li>Cancellation less than 5 days before departure will be charge 75% of tour price.</li>\r\n	<li>Cancellation made at the date of arrival will be charged 100% cancellation fee</li>\r\n</ol>\r\n'),
	(6, 'PAYMENT', '<ol>\r\n	<li>Please note that the payment must be sent to:&nbsp;<strong>balijawala@gmail.com</strong>.</li>\r\n</ol>\r\n');
/*!40000 ALTER TABLE `ketentuan_tour` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.paket
CREATE TABLE IF NOT EXISTS `paket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wilayah_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `detail_harga` text NOT NULL,
  `harga_termasuk` text NOT NULL,
  `harga_tidak_termasuk` text NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.paket: ~9 rows (approximately)
DELETE FROM `paket`;
/*!40000 ALTER TABLE `paket` DISABLE KEYS */;
INSERT INTO `paket` (`id`, `wilayah_id`, `nama`, `slug`, `slider_id`, `gambar`, `detail_harga`, `harga_termasuk`, `harga_tidak_termasuk`, `keterangan`) VALUES
	(1, 1, 'Paket 1', 'banyuwangi-paket-1', 0, 'b7b0b-79593-k01_00000077.jpg', '<ul>\r\n	<li>Rp 500.000 (4 s/d 10 Orang)</li>\r\n	<li>Rp 450.000&nbsp;(11&nbsp;s/d 15&nbsp;Orang)</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Comfortable Air-Conditioned Car</li>\r\n	<li>&nbsp;Petrol</li>\r\n	<li>&nbsp;English Speaking Driver</li>\r\n	<li>&nbsp;Parking fee</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Biaya Sopir</li>\r\n</ul>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(2, 1, 'Paket 2', 'banyuwangi-paket-2', 0, '98a9d-kemiren.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(3, 1, 'Paket 3', 'banyuwangi-paket-3', 0, '60491-green-bay-in-banyuwangi.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(4, 1, 'Paket 4', 'banyuwangi-paket-4', 0, '95cfd-alas-purwo-1.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(5, 2, 'Paket 1', 'bali-paket-1', 0, '6844b-top10-attractions-nusa-dua.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(6, 2, 'Paket 2', 'bali-paket-2', 0, 'be583-img_20120504181400_4fa3b9f85d34b.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(7, 2, 'Paket 3', 'bali-paket-3', 0, '137b9-pura-ulun-danu-bratan-temple-bali-bedugul-lake-beratan.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(8, 2, 'Paket 4', 'bali-paket-4', 0, 'd2106-22631671965_4bca944afd_b.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(9, 2, 'Paket 5', 'bali-paket-5', 0, '87d51-1000x500xterasering-sawah-tegalalang-1.jpg.pagespeed.ic.bhohedue5s.jpg', '', '', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n');
/*!40000 ALTER TABLE `paket` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.paket_detail
CREATE TABLE IF NOT EXISTS `paket_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `kegiatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.paket_detail: ~33 rows (approximately)
DELETE FROM `paket_detail`;
/*!40000 ALTER TABLE `paket_detail` DISABLE KEYS */;
INSERT INTO `paket_detail` (`id`, `paket_id`, `nama`, `slug`, `gambar`, `slider_id`, `kegiatan`, `keterangan`) VALUES
	(1, 5, 'Nusa Dua', 'bali-nusa-dua', '8fbd1-nusa-dua-beach-bali.jpg', 0, '1,3,5,6,7', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(2, 5, 'Jimbaran Bay', 'bali-jimbaran-bay', 'd1b00-nirypziymezifeixbuou.jpg', 0, '1,2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(3, 5, 'Pandawa Beach', 'bali-pandawa-beach', 'a99ad-pandawa-beach-bali.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(4, 5, 'Batubulan Trip', 'bali-batubulan-trip', 'f2c33-2259.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(5, 6, 'Telaga Waja', 'bali-telaga-waja', '3e3e4-telaga-waja-rafting-bali-8.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(6, 6, 'Agro Kopi', 'bali-agro-kopi', '5e22d-bali-pulina-agro-wisata.png', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(7, 6, 'Ubud Tour', 'bali-ubud-tour', 'b92e7-bali-ubud-rice-fields.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(8, 7, 'Pura Ulun Danu Baratan (Bedugul)', 'bali-pura-ulun-danu-baratan-bedugul', '19f06-bedugul-bali-indonesia10-boost.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(9, 7, 'Agro Strawberry', 'bali-agro-strawberry', 'e73c2-ciwidey.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(10, 7, 'Twin Waterfall git git', 'bali-twin-waterfall-git-git', 'b0ad5-git-git-twin-waterfalls.jpg', 0, '1,2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(11, 8, 'Uluwatu Temple', 'bali-uluwatu-temple', '309fe-uluwatu-temple1.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(12, 8, 'Padang Beach', 'bali-padang-beach', 'e62dc-fada188cd677ad6193420424c8990e5a.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(13, 8, 'Dreamland Beach', 'bali-dreamland-beach', 'e6c12-7734704960_948e543a1b_b.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(14, 8, 'Kuta Beach', 'bali-kuta-beach', '4c876-harper-kuta-bg-opt.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(15, 9, 'Tanah Lot', 'bali-tanah-lot', '4fc84-tanah-lot-temple2.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(16, 9, 'Jatiluwih Terasiring', 'bali-jatiluwih-terasiring', '31e7d-indonesia-bali-jatiluwih-2414-bali-new.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(17, 9, 'Monkey Forest', 'bali-monkey-forest', '11bcf-sangeh.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(18, 1, 'City Tour', 'banyuwangi-city-tour', 'b206e-sritanjung-malam-hari-1.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(19, 1, 'Ijen Blue fire', 'banyuwangi-ijen-blue-fire', 'ee082-ijen1.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(20, 1, 'Kalipait', 'banyuwangi-kalipait', '145c0-img_akp05.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(21, 2, 'Jampit', 'banyuwangi-jampit', 'beebb-556017db0423bd91068b456d.jpeg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(22, 2, 'Belawan Waterfall', 'banyuwangi-belawan-waterfall', '6c9fb-air-terjun-blawan.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(23, 2, 'Culture Village Kemiren', 'banyuwangi-culture-village-kemiren', '8e0da-barong-ider-bumi-2.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(24, 2, 'Kampung Anyar Waterfall', 'banyuwangi-kampung-anyar-waterfall', '66958-air-terjun-kembar-kampung-anyar-02_large.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(25, 2, 'Baluran Forest', 'banyuwangi-baluran-forest', '60b6e-padang-savana-taman-nasional-baluran-situbondo.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(26, 2, 'Bangsring Underwater', 'bangsring-underwater', '10370-bangsring-ikan-hias.jpg', 0, '3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(27, 3, 'Red Island', 'banyuwangi-red-island', 'd0b20-red_island.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(28, 3, 'Rajegwesi Beach', 'banyuwangi-rajegwesi-beach', '369e1-pantai-rajeg-wesi-banyuwangi-jawa-timur.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(29, 3, 'Greenbay', 'banyuwangi-greenbay', '558ee-teluk-hijau-3d-1-.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(30, 3, 'Sukomade', 'banyuwangi-sukomade', '23c14-sukamade-turtle-beach-tour.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(31, 4, 'Pancur Beach', 'banyuwangi-pancur-beach', '66512-pantai-pancur-banyuwangi-5.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(32, 4, 'Bedul', 'banyuwangi-bedul', '7b1cd-blueflametourbedul.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(33, 4, 'Alas Purwo', 'banyuwangi-alas-purwo', 'c8e68-alas-purwo-1.jpg', 0, '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n');
/*!40000 ALTER TABLE `paket_detail` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.paket_detail_galeri_images
CREATE TABLE IF NOT EXISTS `paket_detail_galeri_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paket_detail_id` int(11) NOT NULL DEFAULT '0',
  `file_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.paket_detail_galeri_images: ~53 rows (approximately)
DELETE FROM `paket_detail_galeri_images`;
/*!40000 ALTER TABLE `paket_detail_galeri_images` DISABLE KEYS */;
INSERT INTO `paket_detail_galeri_images` (`id`, `paket_detail_id`, `file_name`) VALUES
	(25, 4, '11917-batubulan.jpg'),
	(26, 4, '66125-Barong-_Batubulan_Village-_Bali_1601.jpg'),
	(27, 4, '1318a-Desa-batubulan-tari-kecak.jpg'),
	(28, 4, '594eb-the-property.jpg'),
	(30, 4, 'b4149-6103138818_2852d31de8_z.jpg'),
	(31, 4, 'cd9b9-2259.jpg'),
	(32, 4, 'e2c49-12386329-BATUBULAN-BALI-INDONESIA-JUNE-23-Barong-dancers-after-the-traditional-balinese-perfomance-on-June-23-Stock-Photo.jpg'),
	(33, 4, '396f0-tarian-barong-batubulan.jpg'),
	(34, 18, '4172c-0629100620.jpg'),
	(35, 18, '4d197-IMG_20161207_114147.jpg'),
	(36, 18, 'a9016-094-800x600.jpg'),
	(37, 18, 'b8b65-IMG_20161207_114232.jpg'),
	(38, 18, '1fcab-hok-lay-kiong-klenteng-tertua-di-bekasi-_150926150302-274.jpg'),
	(39, 18, '32ccb-Hoo_Tong_Bio_temple-_Banyuwangi-_Indonesia.jpg'),
	(40, 18, 'be0a1-154626_fo1.jpg'),
	(41, 18, 'ce4aa-sritanjung-malam-hari-1.JPG'),
	(42, 18, 'e68c2-taman-sayuwiwit-malam-2.bp_.blogspot.jpg'),
	(43, 18, '04da7-165749_banyuwangigenjah.jpg'),
	(44, 18, '17aec-Screenshot_120914_094247_AM.jpg'),
	(45, 18, '287db-banyuwangi-311.jpg'),
	(46, 18, 'cc74e-suku-osing.jpg'),
	(47, 18, 'e93ea-banyuwangi-325.jpg'),
	(48, 18, '9763f-banyuwangi-327-1.jpg'),
	(49, 5, 'da2bf-d674199d2b94974368f23df677941a1b5adf9288.jpeg'),
	(50, 5, 'e3024-Alam.jpg'),
	(51, 5, 'eb8bc-02-ArunaToursBali-ArunaBhuana-PanoramaBaliTour-BaliRaftingTelagawajaSpaShopping-gallery.jpg'),
	(52, 5, '0cb92-img_20120504181400_4fa3b9f85d34b.jpg'),
	(53, 5, '1afcc-bali-rafting-telaga-waja-3.jpg'),
	(54, 5, '27719-telagawajaraftingbali.jpg'),
	(55, 5, '4456e-telaga-waja-rafting-02.jpg'),
	(56, 5, '8c6d3-DSC_0133.jpg'),
	(57, 5, '2201e-rafting-at-telaga-waja-river-rafting.jpg'),
	(58, 5, 'b1b7c-telaga-waja-rafting-bali-8.jpg'),
	(59, 5, 'c53f9-Telaga-Waja-Rafting-Sobek1.jpg'),
	(60, 5, 'd34cd-telaga-waja-rifer.jpg'),
	(61, 5, 'd9677-maxresdefault.jpg'),
	(62, 5, '1ff45-telaga-waja-rafting-bali-5.jpg'),
	(63, 5, '369ed-telaga-waja-rafting-2.jpg'),
	(64, 5, '401fa-telaga-waja-rafting.jpg'),
	(65, 5, '4d46c-telaga-waja-rafting-05.jpg'),
	(66, 19, '4a3bf-Kawah-Ijen-11.jpg'),
	(67, 19, '5de48-blue-fire-kawah-ijen.jpg'),
	(68, 19, '6e4b1-img_2162.jpg'),
	(69, 19, '71ccb-79593-k01_00000077.jpg'),
	(70, 19, '85ead-Ijen.Crater.Blue_.Fire_..png'),
	(71, 19, '9abd7-ijen1.jpg'),
	(72, 19, 'ecb0b-maxresdefault.jpg'),
	(73, 19, '3f057-75878--1-.jpg'),
	(74, 1, '52aab-f8ae313c5f20f238ed76581dc31e3133.gif'),
	(75, 1, '5fcb8-nusa-dua-water-blow.jpg'),
	(76, 1, 'afce1-Nusa-Dua-42561.jpg'),
	(77, 1, 'bdeba-top10-hotels-nusa-dua_tanjung-benoa.jpg'),
	(78, 1, '0e9e8-nusa-dua-beach-bali.jpg');
/*!40000 ALTER TABLE `paket_detail_galeri_images` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `tipe` enum('small-text','big-text','image','map') DEFAULT 'small-text',
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.settings: ~12 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `title`, `tipe`, `value`) VALUES
	(1, 'img_background', 'image', '81ae1c0d035a672556cbe5ea9191c832.jpg'),
	(2, 'telp', 'small-text', '00-0000-0000'),
	(3, 'whatsapp', 'small-text', '00-0000-0000'),
	(4, 'email', 'small-text', 'balijawala@gmail.com'),
	(5, 'address', 'small-text', 'jl.ngurahrai'),
	(6, 'bbm', 'small-text', 'bh1s1tt'),
	(7, 'slogan', 'small-text', 'Travel to Indonesia and discover the wonders of Indonesia here'),
	(8, 'img_logo', 'image', '2a5f1469365d0f57314a36a6c9219bbb.png'),
	(9, 'meta_description', 'small-text', 'A professional tour organizer offers special Bali Tour packages in the paradise island of Bali with flexible time arrangements, enjoy Bali Tour with trusted, reliable guide and affordable price'),
	(10, 'meta_keywords', 'small-text', 'bali,tour,guide,driver,packages,holidays'),
	(11, 'office_location', 'map', '-8.226356420688829|114.36651706695557'),
	(12, 'contact_us_text', 'big-text', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis consectetur turpis ut bibendum. Cum sociis natoque fajitin penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet molestie nisl. Aliquam sit amet auctor mauris. Donec ipsum urna, suscipit euismod tempus vitae, tincidunt quis leo. Suspendisse nisi lectus, tincidunt ut condimentum eu, pretium a purus. Nullam in quam sed nisi tincidunt convallis nec ac tellus. Quisque sodales lobortis nunc a ultrices.xxxx</p>\n');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.slider
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `default` enum('Ya','Tidak') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.slider: ~2 rows (approximately)
DELETE FROM `slider`;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` (`id`, `nama`, `default`) VALUES
	(1, 'slider 1', 'Ya'),
	(2, 'slider 2', 'Tidak');
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.slider_images
CREATE TABLE IF NOT EXISTS `slider_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) DEFAULT NULL,
  `keterangan` text,
  `file_name` varchar(100) DEFAULT NULL,
  `priority` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.slider_images: ~7 rows (approximately)
DELETE FROM `slider_images`;
/*!40000 ALTER TABLE `slider_images` DISABLE KEYS */;
INSERT INTO `slider_images` (`id`, `slider_id`, `keterangan`, `file_name`, `priority`) VALUES
	(11, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '48419-165749_banyuwangigenjah.jpg', NULL),
	(12, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '617b6-banyuwangi-311.jpg', NULL),
	(13, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '2695b-banyuwangi-325.jpg', NULL),
	(16, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '7a631-17193-7x3.jpg', NULL),
	(17, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '9ee12-2fe36a31328a28a7f85ac29b1507d44d.jpg', NULL),
	(18, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '0f8cf-bali-69390-7134359.jpg', NULL),
	(19, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '7cb84-Culture_kecakdance.jpg', NULL);
/*!40000 ALTER TABLE `slider_images` ENABLE KEYS */;

-- Dumping structure for function db_balijawala.slugify
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `slugify`(
	`dirty_string` varchar(200)
) RETURNS varchar(200) CHARSET latin1
    DETERMINISTIC
BEGIN
    DECLARE x, y , z Int;
    DECLARE temp_string, new_string VarChar(200);
    DECLARE is_allowed Bool;
    DECLARE c, check_char VarChar(1);

    set temp_string = LOWER(dirty_string);

    Set temp_string = replace(temp_string, '&', ' and ');

    Select temp_string Regexp('[^a-z0-9\-]+') into x;
    If x = 1 then
        set z = 1;
        While z <= Char_length(temp_string) Do
            Set c = Substring(temp_string, z, 1);
            Set is_allowed = False;
            If !((ascii(c) = 45) or (ascii(c) >= 48 and ascii(c) <= 57) or (ascii(c) >= 97 and ascii(c) <= 122)) Then
                Set temp_string = Replace(temp_string, c, '-');
            End If;
            set z = z + 1;
        End While;
    End If;

    Select temp_string Regexp("^-|-$|'") into x;
    If x = 1 Then
        Set temp_string = Replace(temp_string, "'", '');
        Set z = Char_length(temp_string);
        Set y = Char_length(temp_string);
        Dash_check: While z > 1 Do
            If Strcmp(SubString(temp_string, -1, 1), '-') = 0 Then
                Set temp_string = Substring(temp_string,1, y-1);
                Set y = y - 1;
            Else
                Leave Dash_check;
            End If;
            Set z = z - 1;
        End While;
    End If;

    Repeat
        Select temp_string Regexp("--") into x;
        If x = 1 Then
            Set temp_string = Replace(temp_string, "--", "-");
        End If;
    Until x <> 1 End Repeat;

    If LOCATE('-', temp_string) = 1 Then
        Set temp_string = SUBSTRING(temp_string, 2);
    End If;

    Return temp_string;
END//
DELIMITER ;

-- Dumping structure for table db_balijawala.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `nama`, `username`, `password`, `email`) VALUES
	(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table db_balijawala.wilayah
CREATE TABLE IF NOT EXISTS `wilayah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `tagline` varchar(200) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_balijawala.wilayah: ~2 rows (approximately)
DELETE FROM `wilayah`;
/*!40000 ALTER TABLE `wilayah` DISABLE KEYS */;
INSERT INTO `wilayah` (`id`, `slider_id`, `nama`, `slug`, `tagline`, `gambar`, `keterangan`) VALUES
	(1, 1, 'BANYUWANGI', 'banyuwangi', 'Sunrise Of Java', '1bf07-79593-k01_00000077.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>\r\n'),
	(2, 2, 'BALI', 'bali', 'The Island of Thousands Temples', '85ad6-tanah-lot-temple2.jpg', '<p>Bali is an Indonesian island known for its forested volcanic mountains, iconic rice paddies, beaches and coral reefs. The island is home to religious sites such as cliffside Uluwatu Temple. To the south, the beachside city of Kuta has lively bars, while Seminyak, Sanur and Nusa Dua are popular resort towns. The island is also known for its yoga and meditation retreats.</p>\r\n');
/*!40000 ALTER TABLE `wilayah` ENABLE KEYS */;

-- Dumping structure for trigger db_balijawala.kegiatan_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `kegiatan_before_insert` BEFORE INSERT ON `kegiatan` FOR EACH ROW BEGIN
	SET NEW.slug = slugify(NEW.nama);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.kegiatan_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `kegiatan_before_update` BEFORE UPDATE ON `kegiatan` FOR EACH ROW BEGIN
	SET NEW.slug = slugify(NEW.nama);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.paket_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `paket_before_insert` BEFORE INSERT ON `paket` FOR EACH ROW BEGIN
   DECLARE nama_wilayah VARCHAR(200);
   SET nama_wilayah = (SELECT nama FROM wilayah WHERE id = NEW.wilayah_id);
	SET NEW.slug = slugify(CONCAT(nama_wilayah,' - ', NEW.nama));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.paket_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `paket_before_update` BEFORE UPDATE ON `paket` FOR EACH ROW BEGIN
	DECLARE nama_wilayah VARCHAR(200);
   SET nama_wilayah = (SELECT nama FROM wilayah WHERE id = NEW.wilayah_id);
	SET NEW.slug = slugify(CONCAT(nama_wilayah,' - ', NEW.nama));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.paket_detail_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `paket_detail_before_insert` BEFORE INSERT ON `paket_detail` FOR EACH ROW BEGIN
   DECLARE nama_wilayah VARCHAR(200);
   SET nama_wilayah = (SELECT c.nama AS wilayah
								FROM paket_detail a
								LEFT JOIN paket b ON a.paket_id = b.id
								LEFT JOIN wilayah c ON b.wilayah_id = c.id
								WHERE a.id = NEW.id);
	SET NEW.slug = slugify(CONCAT(nama_wilayah,'-',NEW.nama));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.paket_detail_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `paket_detail_before_update` BEFORE UPDATE ON `paket_detail` FOR EACH ROW BEGIN
   DECLARE nama_wilayah VARCHAR(200);
   SET nama_wilayah = (SELECT c.nama AS wilayah
								FROM paket_detail a
								LEFT JOIN paket b ON a.paket_id = b.id
								LEFT JOIN wilayah c ON b.wilayah_id = c.id
								WHERE a.id = NEW.id);
	SET NEW.slug = slugify(CONCAT(nama_wilayah,'-',NEW.nama));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.wilayah_before_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `wilayah_before_insert` BEFORE INSERT ON `wilayah` FOR EACH ROW BEGIN
	SET NEW.slug = slugify(NEW.nama);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger db_balijawala.wilayah_before_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `wilayah_before_update` BEFORE UPDATE ON `wilayah` FOR EACH ROW BEGIN
	SET NEW.slug = slugify(NEW.nama);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
