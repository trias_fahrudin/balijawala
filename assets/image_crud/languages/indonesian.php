<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'Upload files here';
$lang['upload-drop-area'] = 'Drop files here to upload';
$lang['upload-cancel'] = 'Batal';
$lang['upload-failed'] = 'gagal';

$lang['loading'] = 'Loading, mohon tunggu...';
$lang['deleting'] = 'Deleting, mohon tunggu...';
$lang['saving_title'] = 'Saving title...';

$lang['list_delete'] = 'Delete';
$lang['alert_delete'] = 'Are you sure that you want to delete this image?';

/* End of file english.php */
/* Location: ./assets/image_crud/languages/english.php */
