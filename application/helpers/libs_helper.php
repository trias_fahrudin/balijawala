<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('get_settings')) {
    function get_settings($title)
    {
        $CI = &get_instance();
        $setting = $CI->db->get_where('settings', array('title' => $title))->row();

        return $setting->value;
    }
}

if (!function_exists('text_limit')) {
    function text_limit($string, $limit)
    {
        $string = strip_tags($string);

        if (strlen($string) > $limit) {
            $stringCut = substr($string, 0, $limit);

            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...' ;
        }
        return $string;
    }
}

if (!function_exists('load_image')) {
    function load_image($image_path, $width, $height,$zoom = 1,$crop = 1)
    {
        // return site_url('timthumb?src='.site_url($image_path).'&h='.$height.'&w='.$width.'&zc=0');
        return site_url('thumb?src='.site_url($image_path).'&size='.$width.'x'.$height . '&zoom=' . $zoom . '&crop=' . $crop);
    }
}

if (!function_exists('slider_default')) {
    function slider_default()
    {
        $CI = &get_instance();

        $default_slider_id = $CI->db->get_where('slider', array('default' => 'Ya'))->row()->id;

        return $CI->db->get_where('slider_images', array('slider_id' => $default_slider_id));
    }
}
