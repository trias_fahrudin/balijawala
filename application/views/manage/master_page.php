
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin Page</title>

    <link href="<?php echo site_url('assets/backend/')?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo site_url('assets/backend/')?>bootstrap/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo site_url('assets/backend/')?>dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo site_url('assets/backend/')?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <?php
      if(isset($css_files)){
        foreach($css_files as $file): ?>
        	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
      	<?php endforeach;
      }else{ ?>
          <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/DataTables/media/css/jquery.dataTables.css'); ?>" />
    <?php }; ?>

    <?php
      if(isset($js_files)){
        foreach($js_files as $file): ?>
          <script src="<?php echo $file; ?>"></script>
        <?php endforeach;
      }else{ ?>
          <script src="<?php echo site_url('assets/backend/jquery/jquery.min.js')?>"></script>
    <?php }; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->

    <!-- <script src="<?php echo site_url('assets/backend/')?>jquery/jquery.min.js"></script> -->
    <script src="<?php echo site_url('assets/backend/')?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url('assets/backend/')?>metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo site_url('assets/backend/')?>dist/js/sb-admin-2.js"></script>

    <script>
      jQuery.fn.extend({
          live: function (event, callback) {
             if (this.selector) {
                  jQuery(document).on(event, this.selector, callback);
              }
          }
      });
    </script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Admin Page</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo ucwords(strtolower($this->session->userdata('user_name')))?> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url('manage/profile')?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <!-- <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('signout')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo site_url('manage/')?>"><i class="fa fa-th-list fa-fw"></i> Dashboard</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('manage/user')?>"><i class="fa fa-th-list fa-fw"></i> Akun User</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('manage/kegiatan')?>"><i class="fa fa-th-list fa-fw"></i> Kegiatan Tour</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('manage/armada')?>"><i class="fa fa-th-list fa-fw"></i> Armada (Mobil, Bis , etc)</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('manage/wilayah')?>"><i class="fa fa-th-list fa-fw"></i> Wilayah</a>
                        </li>


                        <!-- <li>
                            <a href="<?php echo site_url('manage/konten')?>"><i class="fa fa-th-list fa-fw"></i> Konten</a>
                        </li> -->

                        <li>
                            <a href="<?php echo site_url('manage/tour_condition')?>"><i class="fa fa-th-list fa-fw"></i> Ketentuan Tour</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('manage/slider')?>"><i class="fa fa-th-list fa-fw"></i> Slider</a>
                        </li>

                        <!-- <li>
                            <a href="<?php echo site_url('manage/galeri')?>"><i class="fa fa-th-list fa-fw"></i> Galeri</a>
                        </li> -->

                        <li>
                            <a href="<?php echo site_url('manage/settings')?>"><i class="fa fa-th-list fa-fw"></i> Setting</a>
                        </li>



                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $page_title?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <?php echo $breadcrumbs?>
            <?php if(isset($output)){ echo $output; }else{ include $page_name . ".php";} ?>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


</body>

</html>
