<style>
.blog.medium .excerpt {
    margin-bottom: 10px;
    margin-left: 20px;
    display: inline-block;
    width: 700px;
    float: left;
}

</style>
<!-- Header content
   ================================================== -->
<!-- For data-layout, you can choose between a color background like that "#aaa", "blur" or "image" -->
<?php if(!isset($slider)){ ?>
<section id="noslider" class="sixteen columns headerContent" data-layout="blur">
   <div id="blurMask">
      <canvas id="blurCanvas"></canvas>
   </div>
   <div class="headerContentContainer">
      <div class="pageTitle"><?php echo ucwords(strtolower($destination->wilayah) . ' - ' . $destination->destinasi_nama);?></div>
      <!-- <div class="breadCrumbs"><a href="index.html">Home</a> / <a href="#">Latest news</a>  / <span class="highlight">Blog</span></div> -->
   </div>
</section>
<?php }else{ ?>
<section id="slider" class="sixteen columns headerContent">
   <div class="bannercontainer">
      <div class="banner">
         <ul>
            <?php
               foreach ( $slider->result_array() as $slide) { ?>
            <!-- SLIDE -->
            <li data-transition="boxfade" data-slotamount="5"  data-thumb="<?php echo site_url('assets/web/')?>images/other_images/img53.jpg">
               <img src="<?php echo load_image('uploads/' . $slide['file_name'],960,400)?>" alt="">
               <!-- <div class="caption lfl" data-x="30" data-y="248" data-speed="900" data-start="100" data-easing="easeOutExpo">
                  <h1><title>Home</title></h1>
                  </div> -->
               <div class="caption lfl" data-x="30" data-y="290" data-speed="900" data-start="400" data-easing="easeOutExpo">
                  <p class="whitebg"><?php echo wordwrap($slide['keterangan'],60,"<br />\n")?></p>
               </div>
            </li>
            <?php } ?>
         </ul>
         <div class="tp-bannertimer"></div>
      </div>
   </div>
</section>
<?php } ?>
<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1><?php echo ucwords(strtolower($destination->wilayah) . ' - ' . $destination->wilayah_tagline)?></h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Blog items
      ================================================== -->
   <section class="sixteen columns row left-twenty">
      <!-- Start Blog item
         ================================================== -->
      <article class="blog post">
         <?php if($destination->destinasi_gambar !== ""){ ?>
         <img class="scale-with-grid" src="<?php echo load_image('uploads/paket/' . $destination->destinasi_gambar,960,400)?>" alt="" />
         <?php }else{ ?>
         <img class="scale-with-grid" src="<?php echo load_image('uploads/paket/no_images.jpg',960,400)?>" alt="" />
         <?php } ?>
         <!-- Title
            ================================================== -->
         <section class="title clearfix">
            <!-- <div class="blogDate">
               <p>08</p>
               <span>Dec 2012</span>
               <div class="arrow-down"></div>
               </div> -->
            <div class="titleText" style="margin-left: 2px;">
               <h2><?php echo ucwords(strtolower($destination->destinasi_nama))?></h2>
               <p class="blogMeta">Posted by <a href="#">Admin</a></p>
               <div class="lineSeparator"></div>
            </div>
            <!-- End titleText-->
         </section>
         <!-- End title-->
         <!-- Blog content
            ================================================== -->
         <section class="content">
            <!-- Text
               ================================================== -->
            <p><?php echo $destination->destinasi_keterangan?></p>
         </section>
         <!-- End Content -->
      </article>
      <!-- End Blog item -->
      <?php if(isset($kegiatan)){ ?>
      <div class="titleText" style="margin-left: 2px;">
         <h2>Fun Activities</h2>
         <!-- <p class="blogMeta">Posted by <a href="#">Admin</a> / in <a href="#">Animation</a> / 285 <a href="#commentSection">Comments</a></p> -->
         <div class="lineSeparator"></div>
      </div>
      <?php foreach ($kegiatan->result_array() as $activity) { ?>
      <!-- Start Blog item   ================================================== -->
      <article class="blog medium row">
         <?php if($activity['gambar'] !== ""){ ?>
         <img src="<?php echo load_image('uploads/kegiatan/' . $activity['gambar'],225,165)?>" alt="" />
         <?php }else{ ?>
         <img src="<?php echo load_image('uploads/kegiatan/no_images.jpg',225,165)?>" alt="" />
         <?php } ?>
         <!-- Excerpt  ================================================== -->
         <section class="excerpt">
            <div class="excerptText">
               <a href="">
                  <h2><?php echo $activity['nama']?></h2>
               </a>
               <!-- <p class="blogMeta">by <a href="#">Admin</a> / in <a href="#">Photography</a> / 98 <a href="#">Comments</a></p> -->
               <br />
               <?php echo $activity['keterangan'];?>
            </div>
            <!-- buttons  ================================================== -->
            <!-- End buttons-->
         </section>
         <!-- End Excerpt-->
      </article>
      <!-- End Blog item -->
      <div class="lineSeparator"></div>
      <?php } ?>
      <?php } ?>
      <!-- separator    ================================================== -->
      <!-- <div class="lineSeparator sixteen columns row"></div> -->
      <div class="separator large row-fifty"></div>
      <!-- Portfolio single navigation  ================================================== -->
      <?php if(isset($image_galeri)){ ?>

        <section class="isotopeContainer portfolio left-twelve">
        <?php $i = 1;?>
        <?php foreach ($image_galeri->result_array() as $ig) { ?>

             <div class="element onefifth illustration" style="margin-left:30px">
                <div class="portfolioImage">
                   <a class="jackbox" data-group="work1" data-thumbTooltip = "Image Title" data-title=""  data-description="#description_1" href="<?php echo load_image('uploads/paket/galeri_paket/' . $ig['file_name'],911,512)?>">
                      <div class="jackbox-hover jackbox-hover-blur jackbox-hover-magnify"></div>
                      <img width="225" height="170" src="<?php echo load_image('uploads/paket/galeri_paket/' . $ig['file_name'],225,170)?>" alt="" />
                      <span class="portfolioImageOver transparent"></span>
                   </a>
                </div>
                <div class="portfolioText" data-targetURL="">
                   <span class="portfolioTextOver transparent"></span>
                   <!-- <p>Single image example</p> -->
                   <span>- image <?php echo $i;?> -</span>
                </div>
                <span class="portfolioArrow"></span>
                <!-- Sample div used as an item's description, will only appear inside JackBox -->
                <div class="jackbox-description" id="description_1">
                   <!-- <h3>Description Title One</h3>
                   <a href="#">Link</a> ipsum dolor sit amet, consectetur adipiscing elit. In est metus, tincidunt vitae eleifend sit amet, porta a sapien. Fusce in dolor nec purus facilisis dictum. tincidunt sed quam. -->
                </div>
             </div>
        <?php $i++;} ?>
        </section>
      <?php } ?>

      <!-- end isotope container -->
      <!-- <section class="projectNav row">
         <ul>
            <li class="projectName"><a href="#">previous post</a></li>
            <ul>
               <li class="projectPrev"><a href="#"></a></li>
               <li class="separator"></li>
               <li class="projectNext"><a href="#"></a></li>
            </ul>
            <li class="projectName"><a href="#">next post</a></li>
         </ul>
      </section> -->
      <!-- separator  ================================================== -->

      <div class="clearfix row"></div>
   </section>
   <!-- End twelve columns -->
   <!-- Sidebar
      ================================================== -->

   <div class="clearfix"></div>
</section>
<!-- End // main content -->
