<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie6" lang="en">
   <![endif]-->
   <!--[if IE 7 ]>
   <html class="ie7" lang="en">
      <![endif]-->
      <!--[if IE 8 ]>
      <html class="ie8" lang="en">
         <![endif]-->
         <!--[if IE 9 ]>
         <html class="ie9" lang="en">
            <![endif]-->
            <!--[if (gte IE 9)|!(IE)]><!-->
            <html lang="en">
               <!--<![endif]-->
               <head>
                  <!-- Basic Page Needs
                     ================================================== -->
                  <!-- <meta charset="utf-8"> -->
                  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

                  <title>Bali Banyuwangi trip</title>
                  <meta name="description" content="<?php echo get_settings('meta_description')?>">
                  <meta name="keywords" content="<?php echo get_settings('meta_keywords')?>">

                  <meta name="author" content="WiseGuys">
                  <!--[if lt IE 9]>
                  <link href="jackbox/css/jackbox-ie8.css" rel="stylesheet" type="text/css" />
                  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                  <![endif]-->
                  <!--[if gt IE 8]>
                  <link href="jackbox/css/jackbox-ie9.css" rel="stylesheet" type="text/css" />
                  <![endif]-->
                  <!-- Mobile Specific Metas
                     ================================================== -->
                  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                  <!-- CSS
                     ================================================== -->
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>rs-plugin/css/settings.css"/>
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>css/base.css"/>
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>css/skeleton.css"/>
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>css/layout.css"/>
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>jackbox/css/jackbox_hovers.css"/>
                  <link rel="stylesheet" href="<?php echo site_url('assets/web/')?>jackbox/css/jackbox.css"/>
                  <!-- Colors -->
                  <link id="colorTheme" rel='stylesheet' href='<?php echo site_url('assets/web/')?>css/colors/blueTheme.css'/>

                  <style>
                    header .logo {
                        background:
                          url('<?php echo site_url('assets/web/images/icons/white/logo.png')?>') no-repeat left center,
                          url('<?php echo load_image('uploads/' . get_settings('img_logo'),120,120)?>') top center no-repeat;
                          background-blend-mode: soft-light;
                          /*background-position: no-repeat left center,left center no-repeat;*/
                          /*background-size: 16px 16px, cover;*/
                      }

                      footer .footerLogo{
                        background:
                          url('<?php echo site_url('assets/web/images/footerLogo.png')?>') no-repeat left center,
                          url('<?php echo load_image('uploads/' . get_settings('img_logo'),120,120)?>') top center no-repeat;
                          background-blend-mode: soft-light;
                          /*background-position: no-repeat left center,left center no-repeat;*/
                          /*background-size: 16px 16px, cover;*/
                      }
                  </style>
                  <!-- JS  ================================================== -->
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery-ui.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/tiny.accordion.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jacked.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.easing.1.3.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/slackBlur.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/ddsmoothmenu.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.touchSwipe.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.tweet.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.isotope.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>jackbox/js/libs/jquery.address-1.5.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>jackbox/js/jackbox-swipe.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>jackbox/js/jackbox.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>jackbox/js/libs/StackBlur.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/tipsy/jquery.tipsy.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.flexslider.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/jquery.fitvids.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
                  <script type="text/javascript" src="<?php echo site_url('assets/web/')?>js/wiseguys.min.js"></script>
                  <!-- Favicons
                     ================================================== -->
                  <link rel="shortcut icon" href="<?php echo site_url('assets/web/')?>images/favicon.ico">
                  <link rel="apple-touch-icon" href="<?php echo site_url('assets/web/')?>images/apple-icon.png">
                  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url('assets/web/')?>images/apple-icon-72x72.png">
                  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url('assets/web/')?>images/apple-icon-114x114.png">
               </head>
               <body>
                  <div class="headerBg"></div>
                  <!-- Primary Page Layout   ================================================== -->


                  <div class="container home" data-backgroundImage="<?php echo  load_image('uploads/' . get_settings('img_background'),1400,900)?>">

                     <header class="sixteen columns">
                        <a href="<?php echo site_url()?>">
                           <div class="logo"></div>
                        </a>
                        <div id="google_translate_element" style="float:right"></div>

                        <nav class="mainmenu">
                           <div id="smoothmenu" class="ddsmoothmenu">
                              <ul>
                                 <li>
                                    <a href="<?php echo site_url()?>" class="<?php echo in_array($current_page,array('index')) ? 'current':''?>">HOME</a>
                                 </li>
                                 <?php
                                 $wilayah = $this->db->get('wilayah');
                                //  $region_page = $this->uri->segment(3);
                                 foreach ($wilayah->result_array() as $wil) { ?>
                                   <li>
                                      <a href="<?php echo site_url('web/region/' . $wil['slug'])?>" class="<?php echo in_array($current_page,array($wil['nama'])) ? 'current':''?>"><?php echo $wil['nama']?></a>
                                      <ul>
                                        <?php
                                        $paket = $this->db->get_where('paket',array('wilayah_id' => $wil['id']));
                                        foreach ($paket->result_array() as $pak) { ?>
                                          <li>
                                             <a href="<?php echo site_url('web/tour_package/' . $pak['slug'] )?>"><?php echo strtoupper( $pak['nama'] )?></a>
                                          </li>
                                        <?php } ?>
                                      </ul>
                                    </li>
                                 <?php } ?>
                                 <li>
                                   <a href="<?php echo site_url('web/tour_condition')?>" class="<?php echo in_array($current_page,array('tour_condition')) ? 'current':''?>">TOUR CONDITION</a>
                                 </li>

                                 <li>
                                   <a href="#" class="<?php echo in_array($current_page,array('transportation','contact_us')) ? 'current':''?>">ABOUT US</a>
                                   <ul>
                                     <li>
                                         <a href="<?php echo site_url('web/transportation')?>">TRANSPORTATION</a>
                                     </li>
                                     <li>
                                       <a href="<?php echo site_url('web/contact_us')?>">CONTACT US</a>
                                     </li>
                                   </ul>
                                 </li>

                              </ul>
                              <br style="clear: left" />
                           </div>

                           <!-- end ddsmoothmenu -->
                           <!-- Responsive Menu
                              ================================================== -->
                           <form action="#" method="post">
                              <select>
                                 <option value="">Navigation</option>
                              </select>
                           </form>

                        </nav>
                        <span id="menuShadow"></span>
                        <span id="submenuArrow"><span class="arrow-up"></span></span>

                     </header>
                     <!-- Slider
                        ================================================== -->
                    <?php include $page_name . ".php"?>
                     <!-- End // main content -->
                     <!-- Start footer -->
                     <footer>
                        <div class="footerBgFull"></div>
                        <div class="subFooterBgFull"></div>
                        <div class="arrow-down"></div>
                        <!-- Top footer
                           ================================================== -->
                        <section class="footerTop sixteen columns">
                           <div class="footerTopWrapper">
                              <div class="onefourth logoFooter">
                                 <a href="<?php echo site_url()?>">
                                    <div class="footerLogo"></div>
                                 </a>
                                 <p><?php echo get_settings('slogan')?></p>
                              </div>
                              <div class="onefourth contactDetails">
                                 <h4>KONTAK</h4>
                                 <ul class="footerContacts">
                                    <li class="footerAddress"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'address'),'value')?></li>
                                    <li class="footerPhone"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'telp'),'value')?></li>
                                    <li class="footerWhatsapp"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'whatsapp'),'value')?></li>
                                    <li class="footerBbm"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'bbm'),'value')?></li>

                                    <li class="footerMail"><a href="mailto:<?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'email'),'value')?>"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'email'),'value')?></a></li>
                                 </ul>
                                 <ul class="socialIcons">
                                    <li class="vimeo"><a href="#">Vimeo</a></li>
                                    <li class="facebook"><a href="#">Facebook</a></li>
                                    <li class="linkedin"><a href="#">LinkedIn</a></li>
                                    <li class="twitter"><a href="#">Twitter</a></li>
                                    <li class="pinterest"><a href="#">Pinterest</a></li>
                                    <li class="flickr"><a href="#">Flickr</a></li>
                                 </ul>
                              </div>
                              <!-- <div class="onefourth aboutFooter">
                                 <h4>ABOUT WISE GUYS</h4>
                                 <ul class="footerAbout">
                                    <li><a href="#">The story behind our company</a></li>
                                    <li><a href="#">Our clients testimonials</a></li>
                                    <li><a href="#">An offer you can�t refuse</a></li>
                                    <li><a href="#">Some of our best works</a></li>
                                 </ul>
                              </div> -->
                              <!-- <div class="onefourth footerTweets">
                                 <h4>LATEST TWEETS</h4>
                                 <div class="tweets"></div>
                              </div> -->
                           </div>
                        </section>
                        <!-- End // Footer top -->
                        <!-- Sub footer
                           ================================================== -->
                        <section class="subFooter">
                           <ul class="footerMenu">
                              <li><a href="<?php echo site_url()?>">BERANDA</a></li>

                              <?php foreach ($wilayah->result_array() as $wil) { ?>
                              <li><a href="<?php echo site_url('web/region/' . $wil['slug'] )?>"><?php echo $wil['nama']?></a></li>
                              <?php } ?>
                              <li><a href="<?php echo site_url('web/tour_condition')?>">KETENTUAN TOUR</a></li>
                              <li><a href="<?php echo site_url('web/contact_us')?>">HUBUNGI KAMI</a></li>
                           </ul>
                           <span class="copyright">Copyright &copy; bali-banyuwangi-trip.com <?php echo date('Y')?></span>
                        </section>
                     </footer>
                     <!-- /End footer -->
                  </div>
                  <!-- /End container -->
                  <!-- End Document
                     ================================================== -->

                  <script>
                      var hb = $('body').find('.headerBg');
                      var cb = $('body').find('.contentBgFull');
                      var fb = $('body').find('.footerBgFull');
                      var sfb = $('body').find('.subFooterBgFull');
                      var hcb = $('body').find('.headerContentBg');

                      // hb.removeClass('headerBg');
          						cb.css('visibility', 'hidden');
          						fb.removeClass('footerBgFull');
          						sfb.removeClass('subFooterBgFull');
          						hcb.css('visibility', 'hidden');

                      // hb.addClass('headerBg');
          						// cb.css('visibility', 'visible');
          						// fb.addClass('footerBgFull');
          						// sfb.addClass('subFooterBgFull');
          						// hcb.css('visibility', 'visible');
                  </script>
                  <script type="text/javascript">
                    function googleTranslateElementInit() {
                      new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ar,en,id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
                    }
                  </script>
                  <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

               </body>
            </html>
