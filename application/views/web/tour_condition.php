<!-- Header content
   ================================================== -->
<!-- For data-layout, you can choose between a color background like that "#aaa", "blur" or "image" -->
<section id="noslider" class="sixteen columns headerContent" data-layout="blur">
   <div id="blurMask">
      <canvas id="blurCanvas"></canvas>
   </div>
   <div class="headerContentContainer">
      <div class="pageTitle">Tour Condition</div>
      <!-- <div class="breadCrumbs"><a href="index.html">Home</a> / <a href="#">Features</a> / <span class="highlight">Fearure FAQ</span></div> -->
   </div>
</section>
<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1>Tour Condition</h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Portfolio items
      ================================================== -->
   <section class="twelve columns clearfix">
      <section class="faq row" data-toggle="true">
         <?php foreach ($faq->result_array() as $faq_item) { ?>
         <article class="row">
            <div class="question" style="color:#000000"><?php echo $faq_item['pertanyaan']?></div>
            <p><?php echo $faq_item['jawaban']?></p>
            <div class="separator"></div>
         </article>
         <?php } ?>
      </section>
   </section>
   <!-- End // main content -->
   <aside class="sidebar four columns">
      <!-- side menu
         ================================================== -->
      <section class="contactInfo row">
         <h4>CONTACT DETAILS</h4>
         <article class="contactInfoItem">
            <header>
               <div>Alamat</div>
               <div class="headerBg"></div>
            </header>
            <ul>
               <li>
                  <?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'address'),'value')?>
               </li>
            </ul>
         </article>
         <article class="contactInfoItem">
            <header>
               <div>Kontak</div>
               <div class="headerBg"></div>
            </header>
            <ul class="footerContacts">
               <li class="footerPhone"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'telp'),'value')?></li>
               <li class="footerWhatsapp"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'whatsapp'),'value')?></li>
               <li class="footerBbm"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'bbm'),'value')?></li>
            </ul>
         </article>
         <article class="contactInfoItem">
            <header>
               <div>Email</div>
               <div class="headerBg"></div>
            </header>
            <ul>
               <li><a href="<?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'email'),'value')?>"><?php echo $this->Basecrud_m->get_one_val('settings',array('title' => 'email'),'value')?></a></li>
            </ul>
         </article>
      </section>
      <!-- <section class="contactInfo row">
         <h4>CONTACT BY DEPARTMENT</h4>
         <ul class="contactSidemenu">
            <li><a href="#">Jessica Donovan</a> - management</li>
            <li><a href="#">Michael Thompson</a> - development</li>
            <li><a href="#">Jennifer Taylor</a> - finance</li>
         </ul>
      </section> -->
   </aside>
   <div class="clearfix"></div>
</section>
<!-- End // main content -->
