<section id="slider" class="sixteen columns headerContent">
   <div class="bannercontainer">
      <div class="banner">
         <ul>
           <?php
           $slider = slider_default();
           foreach ( $slider->result_array() as $slide) { ?>
             <!-- SLIDE -->
             <li data-transition="boxfade" data-slotamount="5"  data-thumb="<?php echo site_url('assets/web/')?>images/other_images/img53.jpg">
                <img src="<?php echo load_image('uploads/' . $slide['file_name'],960,400)?>" alt="">
                <!-- <div class="caption lfl" data-x="30" data-y="248" data-speed="900" data-start="100" data-easing="easeOutExpo">
                   <h1>title</h1>
                </div> -->
                <div class="caption lfl" data-x="30" data-y="290" data-speed="900" data-start="400" data-easing="easeOutExpo">
                   <p class="whitebg">
                     <?php echo wordwrap($slide['keterangan'],60,"<br />\n")?>
                   </p>
                </div>
             </li>

           <?php } ?>
         </ul>
         <div class="tp-bannertimer"></div>
      </div>
   </div>
</section>
<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1><?php echo get_settings('slogan')?></h1>
      <!-- <div class="breadCrumbs">
        <a href="index.html">Beranda</a> /
        <a href="#">Latest news</a>  /
        <a href="#">Blog</a>  /
        <span class="highlight">Lorem ipsum</span>
      </div> -->
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Teasers
      ================================================== -->
   <section class="row offset-by-one">
      <div class="onethird">
         <h4>CREATIVE DESIGN AND CONCEPT</h4>
         <p>Suspendisse potenti. Sed placerat mauris quis arcu hendrerit aliquam. Aenean vitae nisl magna. Aliquam elit sem, posuere a vestibulum vitae, hendrerit velaris.</p>
      </div>
      <div class="onethird">
         <h4>FRIENDLY AND INTUITIVE</h4>
         <p>Aenean at facilisis augue. Curabitur sollicitu dinasi egestas suscipit. Praesent vitae eros purus, sit amet tincidunt metus. Cras id bibendum nisl atum bit faelis.</p>
      </div>
      <div class="onethird last">
         <h4>SERIOUSLY AWESOME</h4>
         <p>Sed lobortis, leo eu ullamcorper semper, erat diam ornare erat, vitae sagittis mauris lorem vel quamires. Mauris imperdiet eleifend varius fusce dorim metaus.</p>
      </div>
   </section>
   <!-- Featured works
      ================================================== -->
   <section class="left-twenty sixteen columns">
      <!-- Start section header -->
      <div class="sectionHeader row">
         <div class="sectionHeadingWrap">
            <span class="sectionHeading">TOUR PACKAGE</span>
         </div>
         <!-- Carousel navigation -->
         <div class="carouselNav">
            <div class="carouselPrevious"></div>
            <div class="carouselNext"></div>
         </div>
      </div>
      <!-- /End section header -->
      <!-- Start carousel large-->
      <div class="carouselWrapper large">
         <ul class="carousel portfolio" data-autoPlay="true" data-autoDelay="4000">
            <?php foreach ($tour_package->result_array() as $package) { ?>
              <!-- Start carousel item portfolio -->
              <li>
                 <figure>
                   <?php $nama_wilayah = $this->Basecrud_m->get_one_val('wilayah',array('id' => $package['wilayah_id']),'nama')?>
                   <?php if($package['gambar'] !== ""){ ?>
                    <a class="jackbox" data-group="featured_works" data-thumbTooltip = "<?php echo $nama_wilayah . ' - ' . $package['nama'] ?>" data-title="<a href='<?php echo site_url('web/tour_package/'. $package['slug'])?>'><?php echo $nama_wilayah . ' - ' . $package['nama'] ?></a>"  data-description="#description_1" href="<?php echo load_image('uploads/paket/' . $package['gambar'],911,512)?>">
                   <?php }else{ ?>
                    <a class="jackbox" data-group="featured_works" data-thumbTooltip = "<?php echo $nama_wilayah . ' - ' . $package['nama'] ?>" data-title="<a href='<?php echo site_url('web/tour_package/'. $package['slug'])?>'><?php echo $nama_wilayah . ' - ' . $package['nama'] ?></a>"  data-description="#description_1" href="<?php echo load_image('uploads/paket/no_images.jpg',911,512)?>">
                   <?php } ?>

                       <div class="jackbox-hover jackbox-hover-blur jackbox-hover-magnify"></div>
                       <?php if($package['gambar'] !== ""){ ?>
                        <img src="<?php echo load_image('uploads/paket/' . $package['gambar'],225,170)?>" alt="" />
                       <?php }else{ ?>
                         <img src="<?php echo load_image('uploads/paket/no_images.jpg',225,170)?>" alt="" />
                       <?php } ?>
                        <span class="portfolioImageOver transparent"></span>
                    </a>
                 </figure>
                 <article data-targetURL="<?php echo site_url('web/tour_package/' . $package['slug'])?>">
                    <p>
                      <?php echo $nama_wilayah . ' - ' . $package['nama'] ?>
                    </p>
                    <span>- <?php echo $nama_wilayah ?> -</span>
                 </article>
                 <!-- Sample div used as an item's description, will only appear inside JackBox -->
                 <div class="jackbox-description" id="description_1">
                    <h3><?php echo $nama_wilayah . ' - ' . $package['nama'] ?></h3>
                    <!-- <a href="#">Link</a> ipsum dolor sit amet, consectetur adipiscing elit. In est metus, tincidunt vitae eleifend sit amet, porta a sapien. Fusce in dolor nec purus facilisis dictum. tincidunt sed quam. -->
                    <?php echo $package['keterangan']?>
                 </div>
                 <span class="carouselArrow"></span>
              </li>
              <!-- /End carousel item portfolio -->
            <?php } ?>
         </ul>
         <div class="clearfix"></div>
      </div>
      <!-- /End carousel -->
   </section>
   <!-- End // Featured works -->

   <!-- Latest activity Posts   ================================================== -->
   <?php if(isset($available_activity)){ ?>
     <section class="left-twenty sixteen columns">
        <!-- Start section header -->
        <div class="sectionHeader row">
           <div class="sectionHeadingWrap">
              <span class="sectionHeading">ACTIVITY</span>
           </div>
           <!-- Carousel navigation-->
           <div class="carouselNav">
              <div class="carouselPrevious"></div>
              <div class="carouselNext"></div>
           </div>
        </div>
        <!-- /End section header -->

        <!-- Start carousel large-->
        <div class="carouselWrapper large">
          <ul class="carousel blog" data-autoPlay="false" data-autoDelay="5000">

            <?php foreach ($available_activity->result_array() as $activity) { ?>
              <!-- Start carousel item blog -->
              <li>
                 <img width="225" height="165" src="<?php echo load_image("uploads/kegiatan/" . $activity['gambar'],225,165) ?>" alt="" />

                 <article>
                    <a href="<?php echo site_url('web/activity/' . $activity['slug'])?>">
                       <h4><?php echo $activity['nama']?></h4>
                    </a>
                    <p class="blogMeta">by <a href="#">Admin</a></p>
                    <p style="text-align:justify">
                      <?php echo text_limit(strip_tags(str_replace("&nbsp;", ' ', $activity['keterangan'])),210)?>
                        <a class="highlight" href="<?php echo site_url('web/activity/' . $activity['slug'])?>">Read more</a>
                    </p>
                 </article>
              </li>
              <!-- /End carousel item blog -->
            <?php } ?>
          </ul>
          <!-- <div class="clearfix"></div> -->
       </div>
     </section>
     <!-- End // Latest Blog Posts -->
   <?php } ?>

   

   <!-- Other cool stuff  ================================================== -->

   <!-- End // Other cool stuff -->
</section>
