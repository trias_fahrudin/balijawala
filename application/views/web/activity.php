<style>
.blog.medium .excerpt {
    margin-bottom: 10px;
    margin-left: 20px;
    display: inline-block;
    width: 700px;
    float: left;
}
</style>
<!-- Header content  ================================================== -->

<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1><?php echo ucwords($activity_detail->nama) ?></h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Blog items
      ================================================== -->
   <section class="sixteen columns row left-twenty">
      <!-- Start Blog item
         ================================================== -->
      <article class="blog post">
        <?php if($activity_detail->gambar !== ""){ ?>
         <img class="scale-with-grid" src="<?php echo load_image('uploads/kegiatan/' . $activity_detail->gambar,960,400)?>" alt="" />
        <?php }else{ ?>
          <img class="scale-with-grid" src="<?php echo load_image('uploads/kegiatan/no_images.jpg',960,400)?>" alt="" />
        <?php } ?>
         <!-- Title
            ================================================== -->
         <section class="title clearfix">
            <!-- <div class="blogDate">
               <p>08</p>
               <span>Dec 2012</span>
               <div class="arrow-down"></div>
               </div> -->
            <div class="titleText" style="margin-left: 2px;">
               <h2><?php echo ucwords($activity_detail->nama) ?></h2>
               <p class="blogMeta">Posted by <a href="#">Admin</a></p>
               <div class="lineSeparator"></div>
            </div>
            <!-- End titleText-->
         </section>
         <!-- End title-->
         <!-- Blog content
            ================================================== -->
         <section class="content">
            <!-- Text
               ================================================== -->
            <p><?php echo $activity_detail->keterangan?></p>

         </section>
         <!-- End Content -->
      </article>
      <!-- End Blog item -->

      <div class="titleText" style="margin-left: 2px;">
         <h2>You can find this activity on tour packages below</h2>
         <!-- <p class="blogMeta">Posted by <a href="#">Admin</a> / in <a href="#">Animation</a> / 285 <a href="#commentSection">Comments</a></p> -->
         <div class="lineSeparator"></div>
      </div>
      <?php foreach ($package_details->result_array() as $pd) { ?>
      <!-- Start Blog item   ================================================== -->
      <article class="blog medium row">
        <?php if($pd['gambar'] !== ""){ ?>
         <img src="<?php echo load_image('uploads/paket/' . $pd['gambar'],225,165)?>" alt="" />
        <?php }else{ ?>
          <img src="<?php echo load_image('uploads/paket/no_images.jpg',225,165)?>" alt="" />
        <?php } ?>
         <!-- Excerpt  ================================================== -->
         <section class="excerpt">
            <div class="excerptText">
               <a href="<?php echo site_url('web/tour_package/' . $pd['slug'])?>">
                  <h2><?php echo ucwords(strtolower($pd['nama']))?></h2>
               </a>
               <!-- <p class="blogMeta">by <a href="#">Admin</a> / in <a href="#">Photography</a> / 98 <a href="#">Comments</a></p> -->
               <br />
               <?php echo text_limit($pd['keterangan'],500);?>
            </div>
            <!-- buttons  ================================================== -->
            <section class="buttons">
               <ul class="customButtons">
                  <li class="button readmore"><a class="highlight" href="<?php echo site_url('web/tour_package/' . $pd['slug'])?>">Read more</a></li>
               </ul>
            </section>
            <!-- End buttons-->
         </section>
         <!-- End Excerpt-->
      </article>
      <!-- End Blog item -->
      <?php } ?>



      <!-- End // Latest Blog Posts -->
      <div class="clearfix"></div>

   </section>
   <!-- End twelve columns -->
   <!-- Sidebar
      ================================================== -->

   <div class="clearfix"></div>
</section>
<!-- End // main content -->
