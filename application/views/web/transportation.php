<style>
.blog.medium .excerpt {
    margin-bottom: 10px;
    margin-left: 20px;
    display: inline-block;
    width: 700px;
    float: left;
}
</style>
<!-- Header content
   ================================================== -->
<!-- For data-layout, you can choose between a color background like that "#aaa", "blur" or "image" -->

<section id="noslider" class="sixteen columns headerContent" data-layout="blur">
   <div id="blurMask">
      <canvas id="blurCanvas"></canvas>
   </div>
   <div class="headerContentContainer">
      <div class="pageTitle">Transportation</div>
      <!-- <div class="breadCrumbs"><a href="index.html">Home</a> / <a href="#">Latest news</a>  / <span class="highlight">Blog</span></div> -->
   </div>
</section>


<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1>Transportation</h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Blog items
      ================================================== -->
   <section class="sixteen columns row left-twenty">
      <!-- Start Blog item
         ================================================== -->
      <article class="blog post">
        <img class="scale-with-grid" src="<?php echo load_image('uploads/armada/default-car-images.jpg',860,300,1,0)?>" alt="" />
        <!-- Blog content   ================================================== -->
        <section class="content">
           <!-- Text
              ================================================== -->
           <p><?php echo get_settings('transportation_page_text')?></p>
        </section>
      </article>

      <!-- End Blog item -->
      <?php foreach ($transportation->result_array() as $trans) { ?>
      <!-- Start Blog item   ================================================== -->
      <article class="blog medium row">
        <?php if($trans['gambar'] !== ""){ ?>
         <img src="<?php echo load_image('uploads/armada/' . $trans['gambar'],225,165,1,0)?>" alt="" />
        <?php }else{ ?>
          <img src="<?php echo load_image('uploads/armada/no_images.jpg',225,165,1,0)?>" alt="" />
        <?php } ?>
         <!-- Excerpt  ================================================== -->
         <section class="excerpt">
            <div class="excerptText">
               <a href="#">
                  <h2><?php echo $trans['nama']?></h2>
               </a>
               <!-- <p class="blogMeta">by <a href="#">Admin</a> / in <a href="#">Photography</a> / 98 <a href="#">Comments</a></p> -->
               <br />
               <?php echo $trans['keterangan'];?>
            </div>
            <!-- buttons  ================================================== -->

            <!-- End buttons-->
         </section>
         <!-- End Excerpt-->
      </article>
      <!-- End Blog item -->
      <hr />
      <?php } ?>

      <!-- End // Latest Blog Posts -->
      <div class="clearfix"></div>

   </section>
   <!-- End twelve columns -->
   <!-- Sidebar
      ================================================== -->

   <div class="clearfix"></div>
</section>
<!-- End // main content -->
