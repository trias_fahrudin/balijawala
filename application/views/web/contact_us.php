<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDy5ePPPOnm2Ix6_MU7SGsUX4QzrHfH1t4&sensor=false"></script>
<script type="text/javascript">
//<![CDATA[

     // global "map" variable
      var map = null;
      var marker = null;


      function initialize() {
        <?php
          $location = explode('|',get_settings('office_location'));
        ?>

        var myLatLng = {lat: <?php echo $location[0]; ;?>, lng: <?php echo $location[1];?>};
        // create the map
        var myOptions = {
          zoom: 15,
          center: new google.maps.LatLng(<?php echo $location[0];?>, <?php echo $location[1];?>),
          mapTypeControl: true,
          mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
          navigationControl: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }


        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
          });
      }


      //]]>

    window.onload = initialize;
</script>
<!-- Header content
   ================================================== -->
<!-- For data-layout, you can choose between a color background like that "#aaa", "blur" or "image" -->
<section id="noslider" class="sixteen columns headerContent" data-layout="blur">
   <div id="blurMask">
      <canvas id="blurCanvas"></canvas>
   </div>
   <div class="headerContentContainer">
      <div class="pageTitle">Contact Us</div>
      <!-- <div class="breadCrumbs"><a href="index.html">Home</a> / Contact us</div> -->
   </div>
</section>
<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1>Contact Us</h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Main content
      ================================================== -->
   <section class="twelve columns clearfix contact">
      <article class="gMap">
        <div id="map_canvas" style="width:100%; height:350px;"></div>

      </article>
      <?php echo get_settings('contact_us_text')?>
      <!-- <article>
         <ul class="socialIcons row">
            <li class="vimeo normal"><a class="tooltip" data-tooltipText="Vimeo" href="#">Vimeo</a></li>
            <li class="facebook normal"><a class="tooltip" data-tooltipText="Facebook" href="#">Facebook</a></li>
            <li class="linkedin normal"><a class="tooltip" data-tooltipText="LinkedIn" href="#">LinkedIn</a></li>
            <li class="twitter normal"><a class="tooltip" data-tooltipText="Twitter" href="#">Twitter</a></li>
            <li class="twitter2 normal"><a class="tooltip" data-tooltipText="Twitter" href="#">Twitter</a></li>
            <li class="pinterest normal"><a class="tooltip" data-tooltipText="Pinterest" href="#">Pinterest</a></li>
            <li class="flickr normal"><a class="tooltip" data-tooltipText="Flickr" href="#">Flickr</a></li>
            <li class="digg normal"><a class="tooltip" data-tooltipText="Digg" href="#">Digg</a></li>
            <li class="yahoo1 normal"><a class="tooltip" data-tooltipText="Yahoo" href="#">yahoo1</a></li>
            <li class="yahoo2 normal"><a class="tooltip" data-tooltipText="Yahoo" href="#">yahoo2</a></li>
            <li class="reddit normal"><a class="tooltip" data-tooltipText="Reddit" href="#">Reddit</a></li>
            <li class="googleplus normal"><a class="tooltip" data-tooltipText="Google plus" href="#">Googleplus</a></li>
            <li class="stumbleupon normal"><a class="tooltip" data-tooltipText="Stumbleupon" href="#">Stumbleupon</a></li>
            <li class="skype normal"><a class="tooltip" data-tooltipText="Skype" href="#">Skype</a></li>
            <li class="deviantart normal"><a class="tooltip" data-tooltipText="Deviantart" href="#">Deviantart</a></li>
         </ul>
      </article> -->
      <div class="divider large"></div>
      <!-- <section class="contactForm">
         <h2>Contact Us</h2>
         <div id="contact">
            <div id="message"></div>
            <form method="post" action="classes/contact.php" name="contactform" id="contactform">
               <fieldset>
                  <input name="name" type="text" id="name" size="30" value="Name*" />
                  <input name="email" type="text" id="email" size="30" value="Email*" />
                  <input name="website" type="text" id="website" size="30" value="Website" />
                  <select name="subject" id="subject">
                     <option value="Subject" selected="selected">Choose a subject</option>
                     <option value="Support">Support</option>
                     <option value="Sale">Sales</option>
                     <option value="Bug">Report a bug</option>
                  </select>
                  <textarea name="comments" cols="40" rows="3" id="comments">Message*</textarea>
                  <input type="submit" class="submit button normal dark" id="submit" value="SEND MESSAGE" />
                  <p class="verifyText">Are you human?<span class="required">*</span></p>
                  <label for="verify" accesskey="V" id="verifyImage"><img src="classes/image.php" alt="Image verification" border="0"/></label>
                  <input name="verify" type="text" id="verify" size="6" value="" style="width: 50px;" /><br /><br />
               </fieldset>
            </form>
         </div>
      </section> -->
   </section>
   <!-- End // main content -->
   <aside class="sidebar four columns">
      <!-- side menu
         ================================================== -->
      <section class="contactInfo row">
         <h4>CONTACT DETAILS</h4>
         <article class="contactInfoItem">
            <header>
               <div>Alamat</div>
               <div class="headerBg"></div>
            </header>
            <ul>
               <li>
                  <?php echo get_settings('address')?>
               </li>
            </ul>
         </article>
         <article class="contactInfoItem">
            <header>
               <div>Kontak</div>
               <div class="headerBg"></div>
            </header>
            <ul class="footerContacts">
               <li class="footerPhone"><?php echo get_settings('telp')?></li>
               <li class="footerWhatsapp"><?php echo get_settings('whatsapp')?></li>
               <li class="footerBbm"><?php echo get_settings('bbm')?></li>
            </ul>
         </article>
         <article class="contactInfoItem">
            <header>
               <div>Email</div>
               <div class="headerBg"></div>
            </header>
            <ul>
               <li><a href="<?php echo get_settings('email')?>"><?php echo get_settings('email')?></a></li>
            </ul>
         </article>
      </section>

   </aside>
   <div class="clearfix"></div>
</section>
<!-- End // main content -->
