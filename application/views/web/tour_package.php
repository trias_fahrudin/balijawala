<style>
.blog.medium .excerpt {
    margin-bottom: 10px;
    margin-left: 20px;
    display: inline-block;
    width: 700px;
    float: left;
}
</style>
<!-- Header content
   ================================================== -->
<!-- For data-layout, you can choose between a color background like that "#aaa", "blur" or "image" -->
<?php if(!isset($slider)){ ?>
<section id="noslider" class="sixteen columns headerContent" data-layout="blur">
   <div id="blurMask">
      <canvas id="blurCanvas"></canvas>
   </div>
   <div class="headerContentContainer">
      <div class="pageTitle"><?php echo ucwords(strtolower($package->wilayah . ' - ' . $package->package_nama))?></div>
      <!-- <div class="breadCrumbs"><a href="index.html">Home</a> / <a href="#">Latest news</a>  / <span class="highlight">Blog</span></div> -->
   </div>
</section>
<?php }else{ ?>
<section id="slider" class="sixteen columns headerContent">
   <div class="bannercontainer">
      <div class="banner">
         <ul>
            <?php
               foreach ( $slider->result_array() as $slide) { ?>
            <!-- SLIDE -->
            <li data-transition="boxfade" data-slotamount="5"  data-thumb="<?php echo site_url('assets/web/')?>images/other_images/img53.jpg">
               <img src="<?php echo load_image('uploads/' . $slide['file_name'],960,400)?>" alt="">
               <!-- <div class="caption lfl" data-x="30" data-y="248" data-speed="900" data-start="100" data-easing="easeOutExpo">
                  <h1><title>Home</title></h1>
                  </div> -->
               <div class="caption lfl" data-x="30" data-y="290" data-speed="900" data-start="400" data-easing="easeOutExpo">
                  <p class="whitebg"><?php echo wordwrap($slide['keterangan'],60,"<br />\n")?></p>
               </div>
            </li>
            <?php } ?>
         </ul>
         <div class="tp-bannertimer"></div>
      </div>
   </div>
</section>
<?php } ?>
<section class="mainContent">
   <div class="contentBgFull"></div>
   <!-- Tag Line
      ================================================== -->
   <section id="tagLine" class="sixteen columns row">
      <h1><?php echo ucwords(strtolower($package->wilayah . ' - ' . $package->wilayah_tagline))?></h1>
      <?php echo $breadcrumbs?>
   </section>
   <div id="tagLineShadow" class="sixteen columns"></div>
   <!-- Blog items
      ================================================== -->
   <section class="sixteen columns row left-twenty">
      <!-- Start Blog item
         ================================================== -->
      <article class="blog post">
        <?php if($package->package_gambar !== ""){ ?>
         <img class="scale-with-grid" src="<?php echo load_image('uploads/paket/' . $package->package_gambar,960,400)?>" alt="" />
        <?php }else{ ?>
          <img class="scale-with-grid" src="<?php echo load_image('uploads/paket/no_images.jpg',960,400)?>" alt="" />
        <?php } ?>
         <!-- Title
            ================================================== -->
         <section class="title clearfix">
            <!-- <div class="blogDate">
               <p>08</p>
               <span>Dec 2012</span>
               <div class="arrow-down"></div>
               </div> -->
            <div class="titleText" style="margin-left: 2px;">
               <h2><?php echo ucwords(strtolower($package->wilayah . ' - ' . $package->package_nama))?></h2>
               <p class="blogMeta">Posted by <a href="#">Admin</a></p>
               <div class="lineSeparator"></div>
            </div>
            <!-- End titleText-->
         </section>
         <!-- End title-->
         <!-- Blog content
            ================================================== -->
         <section class="content">
            <!-- Text
               ================================================== -->
            <p><?php echo $package->package_keterangan?></p>


         </section>
         <!-- End Content -->
      </article>
      <!-- End Blog item -->
      <?php foreach ($package_details->result_array() as $pd) { ?>
      <!-- Start Blog item   ================================================== -->
      <article class="blog medium row">
        <?php if($pd['gambar'] !== ""){ ?>
         <img src="<?php echo load_image('uploads/paket/' . $pd['gambar'],225,165)?>" alt="" />
        <?php }else{ ?>
          <img src="<?php echo load_image('uploads/paket/no_images.jpg',225,165)?>" alt="" />
        <?php } ?>
         <!-- Excerpt  ================================================== -->
         <section class="excerpt">
            <div class="excerptText">
               <a href="<?php echo site_url('web/destination/' . $pd['slug'])?>">
                  <h2><?php echo $pd['nama']?></h2>
               </a>
               <!-- <p class="blogMeta">by <a href="#">Admin</a> / in <a href="#">Photography</a> / 98 <a href="#">Comments</a></p> -->
               <br />
               <?php echo text_limit($pd['keterangan'],500);?>
            </div>
            <!-- buttons  ================================================== -->
            <section class="buttons">
               <ul class="customButtons">
                  <li class="button readmore"><a class="highlight" href="<?php echo site_url('web/destination/' . $pd['slug'])?>">Read more</a></li>
               </ul>
            </section>
            <!-- End buttons-->
         </section>
         <!-- End Excerpt-->
      </article>
      <!-- End Blog item -->
      <?php } ?>

      <article>
        <section>
          <div class="actionBox row-thirty" style="border-left:10px solid #00A6A6">
             <!-- <a class="button normal dark reverted" href="#">BUY IT NOW</a> -->
             <h3>PRICE</h3>
             <p><?php echo $package->detail_harga?></p>
          </div>

          <div class="actionBox row-thirty" style="border-left: 10px solid #59b200;">
             <!-- <a class="button normal dark reverted" href="#">BUY IT NOW</a> -->
             <h3>INCLUDED</h3>
             <p><?php echo str_replace('&nbsp;',' ',$package->harga_termasuk)?></p>
          </div>

          <div class="actionBox row-thirty" style="border-left: 10px solid #f60;">
             <!-- <a class="button normal dark reverted" href="#">BUY IT NOW</a> -->
             <h3>EXCLUDED</h3>
             <p><?php echo str_replace('&nbsp;',' ', $package->harga_tidak_termasuk)?></p>
          </div>
        </section>
      </article>
      <!-- End // Latest Blog Posts -->
      <div class="clearfix"></div>

   </section>
   <!-- End twelve columns -->
   <!-- Sidebar
      ================================================== -->

   <div class="clearfix"></div>
</section>
<!-- End // main content -->
