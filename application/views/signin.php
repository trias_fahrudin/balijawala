<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Sign In Page</title>
  <link rel="stylesheet" href="<?php echo site_url('assets/signin/css/style.css')?>">
</head>

<body>
  <!--Google Font - Work Sans-->
<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,700' rel='stylesheet' type='text/css'>

<div class="container">
  <div class="profile profile--open">
    <button class="profile__avatar" id="toggleProfile">
     <img src="<?php echo site_url('assets/signin/images/signin_avatar.png')?>" alt="Avatar" />
    </button>
    <form action="" method="post">
      <div class="profile__form">
        <div class="profile__fields">
          <div class="field">
            <input name="username" type="text" id="fieldUser" class="input" placeholder="Username" required pattern=.*\S.* />
            <!-- <label for="fieldUser" class="label">Username</label> -->
          </div>
          <div class="field">
            <input name="password" type="password" id="fieldPassword" class="input" placeholder="Password" required pattern=.*\S.* />
            <!-- <label for="fieldPassword" class="label">Password</label> -->
          </div>
          <div class="profile__footer">
            <button class="btn" type="submit">Login</button>
          </div>
        </div>
       </div>
    </form>
  </div>
</div>

    <script src="<?php echo site_url('assets/signin/js/index.js')?>"></script>

</body>
</html>
