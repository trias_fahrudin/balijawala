<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| BREADCRUMB CONFIG
| -------------------------------------------------------------------
| This file will contain some breadcrumbs' settings.
|
| $config['crumb_divider']		The string used to divide the crumbs
| $config['tag_open'] 			The opening tag for breadcrumb's holder.
| $config['tag_close'] 			The closing tag for breadcrumb's holder.
| $config['crumb_open'] 		The opening tag for breadcrumb's holder.
| $config['crumb_close'] 		The closing tag for breadcrumb's holder.
|
| Defaults provided for twitter bootstrap 2.0
*/

/*
<ol class="breadcrumb">
  <?php echo $breadcrumbs?>
  <li><a href="#">Home</a></li>
  <li><a href="#">Library</a></li>
  <li class="active">Data</li>
</ol>

*/

$config['default'] = array(
  'tag_open' => '<ol class="breadcrumb">',
  'tag_close' => '</ol>',

  'crumb_divider' => '',

  'crumb_open' => '<li>',
  'crumb_close' => '</li>',

  'crumb_first_open' => '<li>',
  'crumb_first_close' => '</li>',

  'crumb_last_open' => '<li class="active">',
  'crumb_last_close' => '</>',

);

/*
<div class="breadCrumbs">
        <a href="index.html">Beranda</a> /
        <a href="#">Latest news</a>  /
        <a href="#">Blog</a>  /
        <span class="highlight">Lorem ipsum</span>
      </div>
*/
$config['web'] = array(
  'tag_open' => '<div class="breadCrumbs">',
  'tag_close' => '</div>',

  'crumb_divider' => '&nbsp;/&nbsp;',

  'crumb_open' => '',
  'crumb_close' => '',

  'crumb_first_open' => '',
  'crumb_first_close' => '',

  'crumb_last_open' => '<span class="active">',
  'crumb_last_close' => '</span>'


);


//
// $config['tag_open'] = '<ol class="breadcrumb">';
// $config['tag_close'] = '</ol>';
//
// $config['crumb_divider'] = '';
//
// $config['crumb_open'] = '<li>';
//
// $config['crumb_first_open'] = '<li>';
// $config['crumb_last_open'] = '<li class="active">';
//
// $config['crumb_close'] = '</li>';


/* End of file breadcrumbs.php */
/* Location: ./application/config/breadcrumbs.php */
