<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Web extends CI_Controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        // date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->model(array('Basecrud_m'));
        $this->load->library(array('form_validation', 'alert','breadcrumbs'));

        $this->breadcrumbs->load_config('web');

        $this->load->helper(array('url', 'alert','libs'));
    }

    public function _page_output($data = null)
    {
        $this->load->view('web/master_page.php',$data);
    }

    public function index()
    {
        $data['page_name'] = 'home';
        $data['page_title'] = 'Beranda';
        $data['current_page'] = 'index';

        $data['tour_package'] = $this->db->get('paket');
        $available_activity = $this->db->get('kegiatan');

        if($available_activity->num_rows() > 0){
          $data['available_activity'] = $available_activity;
        }

        $this->breadcrumbs->push('Home', '/web');
        $data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->_page_output($data);
    }

    public function transportation(){
      $data['page_name'] = 'transportation';
      $data['page_title'] = 'Transportation';

      $data['current_page'] = 'transportation';

      // $this->db->order_by('rand()');
      // $this->db->limit(1);
      // $query = $this->db->get('armada');
      // $data['random'] = $query->result_array();

      $data['transportation'] = $this->db->get('armada');

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push('Transportation', '/web/transportation');
      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);

    }

    public function tour_package($slug){
      $data['page_name'] = 'tour_package';
      $data['page_title'] = 'Tour Package';

      //checks
      if($this->db->get_where('paket',array('slug' => $slug))->num_rows() == 0){
        redirect('custom_404');
      };

      $this->db->select("a.id,
                         a.nama AS package_nama,
                         a.keterangan AS package_keterangan,
                         a.gambar AS package_gambar,
                         a.detail_harga,
                         a.harga_termasuk,
                         a.harga_tidak_termasuk,
                         b.nama AS wilayah,
                         b.tagline AS wilayah_tagline");
      $this->db->join("wilayah b","a.wilayah_id = b.id","left");
      $this->db->where("a.slug",$slug);
      $data['package'] = $this->db->get("paket a")->row();

      $data['package_details'] = $this->db->get_where('paket_detail',array('paket_id' => $data['package']->id));

      $slider_id = $this->Basecrud_m->get_one_val('paket',array('slug' => $slug),'slider_id');


      if($slider_id != 0){
        $data['slider'] = $this->db->get_where('slider_images',array('slider_id' => $slider_id));
      }

      $data['current_page'] = $data['package']->wilayah;

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push(ucwords(strtolower($data['package']->wilayah )), '/web/region/' . strtolower($data['package']->wilayah));
      $this->breadcrumbs->push(ucwords(strtolower($data['package']->package_nama )), '/web/tour_package/' . $slug);
      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }

    public function destination($slug){
      $data['page_name'] = 'destination';
      $data['page_title'] = 'Destination';

      //checks
      if($this->db->get_where('paket_detail',array('slug' => $slug))->num_rows() == 0){
        redirect('custom_404');
      };

      $this->db->select("a.id,
                         a.nama AS destinasi_nama,
                         a.gambar AS destinasi_gambar,
                         a.keterangan AS destinasi_keterangan,
                         a.kegiatan,a.slider_id,
                         a.slug AS destinasi_slug,
                         b.nama AS paket,
                         b.slug AS paket_slug,
                         c.nama  AS wilayah,
                         c.tagline AS wilayah_tagline ");
      $this->db->join("paket b","a.paket_id = b.id","left");
      $this->db->join("wilayah c","b.wilayah_id = c.id","left");
      $this->db->where("a.slug",$slug);
      $data['destination']  = $this->db->get("paket_detail a")->row();

      $kegiatan = explode(",",$data['destination']->kegiatan);

      if(count(array_filter($kegiatan,'strlen')) > 0){
        $this->db->where_in('id',$kegiatan);
        $data['kegiatan'] = $this->db->get('kegiatan');
      }

      $slider_id = $this->Basecrud_m->get_one_val('paket_detail',array('slug' =>   $slug),'slider_id');

      if($slider_id != 0){
        $data['slider'] = $this->db->get_where('slider_images',array('slider_id' => $slider_id));
      }

      $image_galeri = $this->db->get_where('paket_detail_galeri_images',array('paket_detail_id' => $data['destination']->id));

      if($image_galeri->num_rows() > 0){
        $data['image_galeri'] = $image_galeri;
      }

      $data['current_page'] = $data['destination']->wilayah;

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push(ucwords(strtolower($data['destination']->wilayah )), '/web/region/' . strtolower($data['destination']->wilayah));
      $this->breadcrumbs->push(ucwords(strtolower($data['destination']->paket )), '/web/tour_package/' . $data['destination']->paket_slug);
      $this->breadcrumbs->push(ucwords(strtolower($data['destination']->destinasi_nama )), '/web/destination/' . $data['destination']->destinasi_slug);

      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }

    public function tour_condition(){
      $data['page_name'] = 'tour_condition';
      $data['page_title'] = 'Kondisi Tour';
      $data['current_page'] = 'tour_condition';
      $data['faq'] = $this->db->get('ketentuan_tour');

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push('Tour Package', '/web/tour_condition');

      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }

    public function contact_us(){
      $data['page_name'] = 'contact_us';
      $data['page_title'] = 'Hubungi Kami';
      $data['current_page'] = 'contact_us';

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push('Contact Us', '/web/contact_us');
      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }

    public function activity($slug = null){
      $data['page_name'] = 'activity';
      $data['page_title'] = 'Interesting Activity';

      //checks
      if($this->db->get_where('kegiatan',array('slug' => $slug))->num_rows() == 0){
        redirect('custom_404');
      };

      $data['activity_detail'] = $this->db->get_where('kegiatan',array('slug' => $slug))->row();

      $activity_id = $data['activity_detail']->id;
      $data['package_details'] = $this->db->query(
         "SELECT DISTINCT CONCAT(c.nama,' - ',b.nama) AS nama,b.slug,b.gambar,b.keterangan
          FROM paket_detail a
          LEFT JOIN paket b ON a.paket_id = b.id
          LEFT JOIN wilayah c ON b.wilayah_id = c.id
          WHERE a.kegiatan LIKE '$activity_id,%' OR a.kegiatan LIKE '%$activity_id'");
      $data['current_page'] = 'index';

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push('Activity', '/web/activity');

      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }

    public function region($slug = null){
      $data['page_name'] = 'region';
      $data['page_title'] = 'Wilayah';

      $region_name = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'nama');
      $data['region_name'] = $region_name;
      $data['region_tagline'] = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'tagline');
      $data['region_keterangan'] = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'keterangan');
      $data['region_image'] = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'gambar');


      $wilayah_id = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'id');
      $data['tour_package'] = $this->db->get_where('paket',array('wilayah_id' => $wilayah_id));


      $slider_id = $this->Basecrud_m->get_one_val('wilayah',array('slug' => $slug),'slider_id');

      if($slider_id != 0){
        $data['slider'] = $this->db->get_where('slider_images',array('slider_id' => $slider_id));
      }

      $data['current_page'] = $region_name;

      $this->breadcrumbs->push('Home', '/web');
      $this->breadcrumbs->push(ucwords(strtolower($region_name)), '/web/region/' . strtolower($region_name) );
      $data['breadcrumbs'] = $this->breadcrumbs->show();

      $this->_page_output($data);
    }


}
