<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Signin extends CI_Controller
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->model(array('Basecrud_m'));
        $this->load->library(array('form_validation', 'alert'));
        $this->load->helper(array('url', 'alert'));
    }

    public function index()
    {
        if (!empty($_POST)) {
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == true) {
                $username = $this->input->post('username');
                $password = md5($this->input->post('password'));

                $qry = $this->Basecrud_m->get_where('users', array(
                    'username' => $username,
                    'password' => $password, )
                );

                if ($qry->num_rows() > 0) {

                    //valid user
                    $user = $qry->row();

                    $this->session->set_userdata('user_id', $user->id);
                    $this->session->set_userdata('user_name', $user->username);

                    redirect('manage', 'reload');
                } else {
                    $this->alert->set('errormsg',
                    '<li>Periksa username anda</li>' .
                    '<li>Periksa password anda</li>', true);
                }
            } else {
            }
        }

        $this->load->view('signin');
    }
}
