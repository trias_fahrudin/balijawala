<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Manage extends CI_Controller
{
    private $user_id;

    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set('Asia/Jakarta');

        $this->load->database();
        $this->load->helper(array('url'));
        $this->load->library(array('form_validation', 'session', 'breadcrumbs'));

        $this->breadcrumbs->load_config('default');

        $this->user_id = $this->session->userdata('user_id');

        if (!$this->user_id) {
            redirect('signin', 'refresh');
        }

        $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    }

    public function _page_output($output = null)
    {
        $this->load->view('manage/master_page.php', (array) $output);
    }

    public function index()
    {
        $this->breadcrumbs->push('Dashboard', '/manage');

        $data['breadcrumbs'] = $this->breadcrumbs->show();

        $data['page_name'] = 'beranda';
        $data['page_title'] = 'Dashboard';
        $this->_page_output($data);
    }

    public function user()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('users');
            $crud->set_subject('Data User');

            $state = $crud->getState();

            if ($state === 'edit') {
                $crud->field_type('password', 'hidden');
            } else {
                $crud->field_type('password', 'password');
            }

            $crud->required_fields('email', 'password', 'nama', 'username');
            $crud->callback_before_insert(array($this, 'encrypt_password_callback'));

            $crud->columns('nama', 'email', 'username');

            $crud->unset_read_fields('password');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Akun User', '/manage/user');

            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/user/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/user/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Akun User',
            );
            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
            // var_dump($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function encrypt_password_callback($post_array, $primary_key = null)
    {
        $post_array['password'] = md5($post_array['password']);

        return $post_array;
    }

    public function slider_images()
    {
        try {
            $this->load->library('image_CRUD');
            $this->load->model('Basecrud_m');

            $image_crud = new image_CRUD();

            $image_crud->set_table('slider_images');

            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('file_name');

            $image_crud->set_relation_field('slider_id');
            // $image_crud->set_ordering_field('priority');
            $image_crud->set_image_path('uploads');
            $image_crud->set_title_field('keterangan');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Slider', '/manage/slider');

            $slider_id = $this->uri->segment(3);
            @$nama_slider = $this->Basecrud_m->get_one_val('slider', array('id' => $slider_id), 'nama');

            $this->breadcrumbs->push($nama_slider, '/manage/slider/'.$slider_id);

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Data gambar slider',
            );

            $output = $image_crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    // public function galeri_images()
    // {
    //     try {
    //         $this->load->library('image_CRUD');
    //         $this->load->model('Basecrud_m');
    //
    //         $image_crud = new image_CRUD();
    //
    //         $image_crud->set_table('galeri_images');
    //
    //         $image_crud->set_primary_key_field('id');
    //         $image_crud->set_url_field('file_name');
    //
    //         $image_crud->set_relation_field('galeri_id');
    //         // $image_crud->set_ordering_field('priority');
    //         $image_crud->set_image_path('uploads');
    //         $image_crud->set_title_field('judul');
    //
    //         $this->breadcrumbs->push('Dashboard', '/manage');
    //         $this->breadcrumbs->push('Slider', '/manage/galeri');
    //
    //         $galeri_id = $this->uri->segment(3);
    //         @$nama_galeri = $this->Basecrud_m->get_one_val('galeri', array('id' => $galeri_id), 'nama');
    //
    //         $this->breadcrumbs->push($nama_galeri, '/manage/galeri/'.$galeri_id);
    //
    //         $extra = array(
    //           'breadcrumbs' => $this->breadcrumbs->show(),
    //           'page_title' => 'Data gambar galeri',
    //         );
    //
    //         $output = $image_crud->render();
    //
    //         $output = array_merge((array) $output, $extra);
    //
    //         $this->_page_output($output);
    //     } catch (Exception $e) {
    //         show_error($e->getMessage().' --- '.$e->getTraceAsString());
    //     }
    // }

    public function paket_detail_galeri_images($paket_detail_id)
    {
        try {
            $this->load->library('image_CRUD');
            $this->load->model('Basecrud_m');

            $image_crud = new image_CRUD();

            $image_crud->set_table('paket_detail_galeri_images');

            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('file_name');

            $image_crud->set_relation_field('paket_detail_id');
            $image_crud->set_image_path('uploads/paket/galeri_paket');

            $this->db->select("a.paket_id,a.id,a.nama AS destinasi,
                               b.nama AS nama_paket,
                               c.nama AS nama_wilayah,
                               c.id AS wilayah_id");
            $this->db->join("paket b","a.paket_id = b.id","left");
            $this->db->join("wilayah c","b.wilayah_id = c.id","left");
            $this->db->where("a.id",$paket_detail_id);
            @$rs = $this->db->get("paket_detail a")->row();

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Wilayah', '/manage/wilayah');
            $this->breadcrumbs->push('Paket Tour ' . ucwords(strtolower(@$rs->nama_wilayah)), '/manage/paket/' . @$rs->wilayah_id);
            $this->breadcrumbs->push(@$rs->nama_paket, '/manage/paket_detail/' . @$rs->wilayah_id . '/' . @$rs->paket_id);

            $this->breadcrumbs->push(@$rs->destinasi, '/manage/galeri/'.@$rs->id);

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Data galeri ' . ucwords(strtolower(@$rs->destinasi)),
            );

            $output = $image_crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function slider()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('slider');
            $crud->set_subject('Data Slider');

            $crud->columns('nama', 'daftar_gambar','default');
            $crud->callback_column('daftar_gambar', array($this, '_slider_images'));

            $crud->callback_after_insert(array($this, '_slider_after_insert'));
            $crud->callback_after_update(array($this, '_slider_after_update'));

            $crud->required_fields('nama');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Slider', '/manage/slider');

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/slider/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/slider/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Slider',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    function _slider_after_update($post_array,$primary_key){

      $this->db->where('id !=',$primary_key);
      $this->db->update('slider', array('default' => 'Tidak'));
      return true;
    }

    function _slider_after_insert($post_array,$primary_key){

      $this->db->where('id !=',$primary_key);
      $this->db->update('slider', array('default' => 'Tidak'));
      return true;
    }

    // public function galeri()
    // {
    //     try {
    //         $this->load->library('grocery_CRUD');
    //         $crud = new Grocery_CRUD();
    //
    //         $crud->set_table('galeri');
    //         $crud->set_subject('Data galeri');
    //
    //         $crud->columns('nama', 'daftar_gambar');
    //         $crud->callback_column('daftar_gambar', array($this, '_galeri_images'));
    //
    //         $this->breadcrumbs->push('Dashboard', '/manage');
    //         $this->breadcrumbs->push('Galeri', '/manage/galeri');
    //
    //         $state = $crud->getState();
    //         if ($state === 'edit') {
    //             $this->breadcrumbs->push('Edit', '/manage/galeri/edit');
    //         } elseif ($state === 'add') {
    //             $this->breadcrumbs->push('Tambah', '/manage/galeri/add');
    //         }
    //
    //         $extra = array(
    //           'breadcrumbs' => $this->breadcrumbs->show(),
    //           'page_title' => 'Galeri',
    //         );
    //
    //         $output = $crud->render();
    //
    //         $output = array_merge((array) $output, $extra);
    //
    //         $this->_page_output($output);
    //     } catch (Exception $e) {
    //         show_error($e->getMessage().' --- '.$e->getTraceAsString());
    //     }
    // }

    public function _slider_images($value, $row)
    {
        return "<a href='".site_url('manage/slider_images/'.$row->id)."'>Manage</a>";
    }

    // public function _galeri_images($value, $row)
    // {
    //     return "<a href='".site_url('manage/galeri_images/'.$row->id)."'>Manage</a>";
    // }

    public function _paket_detail_galeri_images($value,$row){
      return "<a href='".site_url('manage/paket_detail_galeri_images/'.$row->id)."'>Manage</a>";
    }

    public function wilayah()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('wilayah');
            $crud->set_subject('Data Wilayah');

            $crud->order_by('nama','ASC');

            $crud->columns('nama', 'paket');
            $crud->field_type('slug', 'readonly');
            $crud->set_field_upload('gambar', 'uploads');

            $crud->callback_column('paket', array($this, '_paket'));

            $crud->set_relation('slider_id', 'slider', 'nama');

            $crud->display_as('slider_id', 'Gambar Slider');

            $crud->required_fields('nama', 'keterangan');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Wilayah', '/manage/wilayah');

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/wilayah/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/wilayah/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Wilayah',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function tour_condition()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('ketentuan_tour');
            $crud->set_subject('Data ketentuan tour');

            $crud->columns('pertanyaan', 'jawaban');

            $crud->required_fields('pertanyaan', 'jawaban');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Ketentuan Tour', '/manage/tour_condition');

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/tour_condition/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/tour_condition/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Ketentuan Tour',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _paket($value, $row)
    {
        return "<a href='".site_url('manage/paket/'.$row->id)."'>Manage</a>";
    }

    public function paket($wilayah_id)
    {
        try {
            $this->load->library('grocery_CRUD');
            $this->load->model('Basecrud_m');

            $crud = new Grocery_CRUD();

            $crud->set_table('paket');
            $crud->set_subject('Data Paket Tour');

            $crud->where('wilayah_id', $wilayah_id);

            $crud->order_by('nama','ASC');

            $crud->field_type('wilayah_id', 'hidden', $wilayah_id);
            $crud->field_type('slug', 'readonly');

            $crud->columns('nama', 'keterangan', 'detail');
            $crud->set_field_upload('gambar', 'uploads/paket');

            $crud->callback_column('detail', array($this, '_paket_detail'));

            $crud->set_relation('slider_id', 'slider', 'nama');

            $crud->display_as('slider_id', 'Gambar Slider');

            $crud->set_lang_string(
                'insert_success_message',
                 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
                 <script type="text/javascript">
                  window.location = "'.site_url(strtolower(__CLASS__).'/paket').'/'.$wilayah_id.'";
                 </script>
                 <div style="display:none">'
            );

            $crud->set_lang_string(
               'update_success_message',
                 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
                 <script type="text/javascript">
                  window.location = "'.site_url(strtolower(__CLASS__).'/paket').'/'.$wilayah_id.'";
                 </script>
                 <div style="display:none">'
            );

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Wilayah', '/manage/wilayah');

            $wilayah = $this->Basecrud_m->get_one_val('wilayah', array('id' => $wilayah_id), 'nama');

            $this->breadcrumbs->push('Paket Tour '.ucwords(strtolower($wilayah)), '/manage/paket/'.$wilayah_id);

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/paket/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/paket/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Paket Tour',
            );
            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _paket_detail($value, $row)
    {
        return "<a href='".site_url('manage/paket_detail/'.$this->uri->segment(3).'/'.$row->id)."'>Manage</a>";
    }

    public function settings($act = null, $param = null)
    {
        $this->load->model(array('Basecrud_m'));
        $this->breadcrumbs->push('Setting', '/settings');

        $data['breadcrumbs'] = $this->breadcrumbs->show();

        if ($act === 'upload') {
            if (!empty($_FILES['img']['name'])) {
                $upload = array();
                $upload['upload_path'] = './uploads';
                $upload['allowed_types'] = 'jpeg|jpg|png';
                $upload['encrypt_name'] = true;

                $this->load->library('upload', $upload);

                if (!$this->upload->do_upload('img')) {
                    $data['msg'] = $this->upload->display_errors();
                } else {
                    $success = $this->upload->data();
                    $value = $success['file_name'];
                    $file_ext = $success['file_ext'];

                    $this->db->where('title', $param);
                    $this->db->update('settings', array('value' => $value, 'tipe' => 'image'));

                    redirect('manage/settings');
                }
            }
        } elseif ($act === 'edt') {
            $value = $this->input->post('value');

            $this->db->where('title', $param);
            $this->db->update('settings', array('value' => $value));

            exit(0);
        }

        $data['setting'] = $this->Basecrud_m->get('settings');
        $data['page_name'] = 'settings';
        $data['page_title'] = 'Data Settings';

        $this->_page_output($data);
    }

    public function paket_detail($wilayah_id, $paket_id)
    {
        try {
            $this->load->library('grocery_CRUD');
            $this->load->model('Basecrud_m');

            $crud = new Grocery_CRUD();

            $crud->set_table('paket_detail');
            $crud->set_subject('Details Paket Tour');

            $crud->where('paket_id', $paket_id);

            $crud->order_by('nama','ASC');

            $crud->field_type('paket_id', 'hidden', $paket_id);
            $crud->columns('nama','kegiatan', 'galeri', 'keterangan');
            $crud->set_field_upload('gambar', 'uploads/paket');

            $crud->callback_column('galeri', array($this, '_paket_detail_galeri_images'));

            $kegiatan = array();
            $keg = $this->db->get('kegiatan');
            foreach ($keg->result_array() as $k) {
              $kegiatan[$k['id']] = $k['nama'];
            }

            $crud->field_type('kegiatan','multiselect',$kegiatan);
            $crud->field_type('slug', 'readonly');

            $crud->set_relation('slider_id', 'slider', 'nama');

            $crud->display_as('slider_id', 'Gambar Slider');

            $crud->set_lang_string(
                'insert_success_message',
                 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
                 <script type="text/javascript">
                  window.location = "'.site_url(strtolower(__CLASS__).'/paket_detail').'/'.$wilayah_id.'/'.$paket_id.'";
                 </script>
                 <div style="display:none">'
            );

            $crud->set_lang_string(
               'update_success_message',
                 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
                 <script type="text/javascript">
                  window.location = "'.site_url(strtolower(__CLASS__).'/paket_detail').'/'.$wilayah_id.'/'.$paket_id.'";
                 </script>
                 <div style="display:none">'
            );

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Wilayah', '/manage/wilayah');

            $wilayah = $this->Basecrud_m->get_one_val('wilayah', array('id' => $wilayah_id), 'nama');
            $this->breadcrumbs->push('Paket Tour '.ucwords(strtolower($wilayah)), '/manage/paket/'.$wilayah_id);

            $paket = $this->Basecrud_m->get_one_val('paket', array('id' => $paket_id), 'nama');
            $this->breadcrumbs->push(ucwords(strtolower($paket)), '/manage/paket_detail/'.$wilayah_id.'/'.$paket_id);

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/paket/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/paket/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Detail Paket Tour',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function kegiatan()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('kegiatan');
            $crud->set_subject('Data Kegiatan tour');

            $crud->order_by('nama','ASC');

            $crud->columns('nama', 'keterangan');
            $crud->set_field_upload('gambar', 'uploads/kegiatan');

            $crud->required_fields('nama');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Kegiatan Tour', '/manage/kegiatan');

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/kegiatan/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/kegiatan/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Kegiatan Tour',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function armada()
    {
        try {
            $this->load->library('grocery_CRUD');
            $crud = new Grocery_CRUD();

            $crud->set_table('armada');
            $crud->set_subject('Data Armada');

            $crud->order_by('nama','ASC');

            $crud->columns('nama', 'keterangan');
            $crud->set_field_upload('gambar', 'uploads/armada');

            $crud->required_fields('nama');

            $this->breadcrumbs->push('Dashboard', '/manage');
            $this->breadcrumbs->push('Armada Tour', '/manage/armada');

            $state = $crud->getState();
            if ($state === 'edit') {
                $this->breadcrumbs->push('Edit', '/manage/armada/edit');
            } elseif ($state === 'add') {
                $this->breadcrumbs->push('Tambah', '/manage/armada/add');
            }

            $extra = array(
              'breadcrumbs' => $this->breadcrumbs->show(),
              'page_title' => 'Armada Tour',
            );

            $output = $crud->render();

            $output = array_merge((array) $output, $extra);

            $this->_page_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function profile()
    {
        $user_id = $this->session->userdata('user_id');

        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            if ($this->form_validation->run() == true) {
                if (!empty($_POST['pass_lama'])) {
                    $password = md5($this->input->post('pass_lama'));

                    $cek_user = $this->db->get_where('users',
                                      array(
                                        'id' => $user_id,
                                        'password' => $password
                                      )
                                );

                    if ($cek_user->num_rows() > 0) {
                        if (empty($_POST['pass_baru']) || empty($_POST['pass_ulangi'])) {
                            $data['msg'] = array(
                                            'content' => 'Password Baru / Ulangi tidak boleh kosong',
                                            'css_class' => 'alert alert-danger', );
                        } else {
                            $pass_baru = $this->input->post('pass_baru');
                            $pass_ulangi = $this->input->post('pass_ulangi');

                            if ($pass_baru !== $pass_ulangi) {
                                $data['msg'] = array(
                                              'content' => 'Password Baru & Ulangi Harus Sama!',
                                              'css_class' => 'alert alert-danger', );
                            } else {
                                $nama = $this->input->post('nama');
                                $email = $this->input->post('email');

                                $this->db->where('id',$user_id);
                                $this->db->update('users',
                                                      array('password' => md5($pass_ulangi),
                                                            'nama' => $nama,
                                                            'email' => $email, ));

                                $data['msg'] = array(
                                              'content' => 'Profile berhasil diupdate',
                                              'css_class' => 'alert alert-success', );
                            }
                        }
                    } else {
                        $data['msg'] = array(
                                      'content' => 'Password Lama Salah',
                                      'css_class' => 'alert alert-danger', );
                    }
                } else {
                    $nama = $this->input->post('nama');
                    $email = $this->input->post('email');

                    $this->db->where('id',$user_id);
                    $this->db->update('users',
                                          array('nama' => $nama,
                                                'email' => $email, ));


                    $data['msg'] = array(
                                  'content' => 'Profile berhasil diupdate',
                                  'css_class' => 'alert alert-success', );
                }
            } else {
                $data['msg'] = array(
                            'content' => validation_errors(),
                            'css_class' => 'alert alert-danger', );
            }

            $data['p'] = $this->db->get_where('users', array('id' => $user_id))->row();
            $data['page_name'] = 'profile';
            $data['page_title'] = 'Profile';
            $data['breadcrumbs'] = $this->breadcrumbs->show();

            $this->_page_output($data);
        } else {
            $data['p'] = $this->db->get_where('users', array('id' => $user_id))->row();
            $data['page_name'] = 'profile';

            $this->breadcrumbs->push('Beranda', '/manage');
            $this->breadcrumbs->push('Profile', '/manage/profile');

            $data['breadcrumbs'] = $this->breadcrumbs->show();
            $data['page_title'] = 'Profile';

            $this->_page_output($data);
        }
    }

    // public function konten()
    // {
    //     try {
    //         $this->load->library('grocery_CRUD');
    //         $crud = new Grocery_CRUD();
    //
    //         $crud->set_table('konten');
    //         $crud->set_subject('Data Konten');
    //
    //         $crud->columns('nama', 'slug');
    //
    //         $crud->field_type('slug', 'readonly');
    //
    //         $crud->unset_add();
    //         $crud->unset_delete();
    //         $crud->unset_print();
    //         $crud->unset_export();
    //
    //         $crud->set_relation('slider_id', 'slider', 'nama');
    //
    //         $crud->display_as('slider_id', 'Gambar Slider');
    //
    //         $this->breadcrumbs->push('Dashboard', '/manage');
    //         $this->breadcrumbs->push('Konten', '/manage/konten');
    //
    //         $state = $crud->getState();
    //         if ($state === 'edit') {
    //             $this->breadcrumbs->push('Edit', '/manage/konten/edit');
    //         } elseif ($state === 'add') {
    //             $this->breadcrumbs->push('Tambah', '/manage/konten/add');
    //         }
    //
    //         $extra = array(
    //           'breadcrumbs' => $this->breadcrumbs->show(),
    //           'page_title' => 'Konten',
    //         );
    //
    //         $output = $crud->render();
    //
    //         $output = array_merge((array) $output, $extra);
    //
    //         $this->_page_output($output);
    //     } catch (Exception $e) {
    //         show_error($e->getMessage().' --- '.$e->getTraceAsString());
    //     }
    // }
}
